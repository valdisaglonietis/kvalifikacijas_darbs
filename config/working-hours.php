<?php
return [
   'weekdays' => [
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday'
    ],
    'duration' => 20,
    'maxWorkHoursCount' => 480,
    'maxWorkHoursIntervalCount' => 365

];