<?php return
    [
        'sex_types' => [
            '0' => 'none',
            '1' => 'man',
            '2' => 'woman',
        ],
        'date_format' => 'd.m.Y',
        'datetime_format' => 'd.m.Y h:i'
    ];
