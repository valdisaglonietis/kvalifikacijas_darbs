<?php

use App\Models\Investigations;

class InvestigationsTest extends TestCase
{
	
	public function testInvestigationsModel(){
      	
      	$InvestigationsCount = Investigations::count();
				
		$Investigations = new Investigations;
        $Investigations->notes      = 'Test Investigations Name';
       
        // SAVE Investigations
        $Investigations->save();
        $InvestigationsID = $Investigations->id;
		
		$this->assertEquals($InvestigationsCount+1, Investigations::count());
		$this->assertEquals('Test Investigations Name', $Investigations->notes);
		
		
		//Update test 
		$Investigations          = Investigations::find($InvestigationsID);
        $Investigations->notes 	= 'Test Investigations Name 2';
        $Investigations->save();
		$this->assertEquals('Test Investigations Name 2', $Investigations->notes);
		
		//Removal test
		$this->assertEquals($InvestigationsCount+1, Investigations::count());
		$Investigations = Investigations::find($InvestigationsID);
		$Investigations->delete();
		$this->assertEquals($InvestigationsCount, Investigations::count());
		
    }


	
}
