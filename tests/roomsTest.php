<?php

use App\Models\Rooms;

class roomsTest extends TestCase
{
	
	public function testRoomsModel(){
      	
      	$RoomsCount = Rooms::count();
				
		$Rooms = new Rooms;
        $Rooms->name      = 'Test room Name';
       
        // SAVE Rooms
        $Rooms->save();
        $RoomsID = $Rooms->id;
		
		$this->assertEquals($RoomsCount+1, Rooms::count());
		$this->assertEquals('Test room Name', $Rooms->name);
		
		
		//Update test 
		$Rooms          = Rooms::find($RoomsID);
        $Rooms->name 	= 'Test room Name 2';
        $Rooms->save();
		$this->assertEquals('Test room Name 2', $Rooms->name);
		
		//Removal test
		$this->assertEquals($RoomsCount+1, Rooms::count());
		$Rooms = Rooms::find($RoomsID);
		$Rooms->delete();
		$this->assertEquals($RoomsCount, Rooms::count());
		
    }


	
}
