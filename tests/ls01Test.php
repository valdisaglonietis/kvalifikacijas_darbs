<?php

use App\User;

class ls01Test extends TestCase
{
	
	private $userID  = 0;
	private $userCount = 0;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserModel(){
      	
      	$userCount = User::count();
				
		$user = new User;
        $user->email      = 'testuser@testuser.lv';
        $user->first_name = 'Test user name';
        $user->last_name  = 'Test user lastname';
        $user->username   = 'testuser@testuser.lv';

        // SAVE USER
        $user->save();
        $userID = $user->id;
		
		$this->assertEquals($userCount+1, User::count());
		$this->assertEquals('testuser@testuser.lv', $user->email);
		
		
		//Update test 
		$user          = user::find($userID);
        $user->email 	= 'testuser2@testuser2.lv';
        $user->save();
		$this->assertEquals('testuser2@testuser2.lv', $user->email);
		
		//Removal test
		$this->assertEquals($userCount+1, User::count());
		$user = user::find($userID);
		$user->delete();
		$this->assertEquals($userCount, User::count());
		
    }
	
	
	public function testUserActivites(){
		$userCount  = UserActivities::count();
		$list 		= UserActivities::all();
		$this->assertEquals($userCount, UserActivities::count());
	}

	
}
