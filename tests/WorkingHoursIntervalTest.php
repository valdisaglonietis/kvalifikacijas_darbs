<?php

use App\Models\WorkingHoursInterval;

class WorkingHoursIntervalTest extends TestCase
{
	
	public function testWorkingHoursIntervalModel(){
      	
      	$WorkingHoursIntervalCount = WorkingHoursInterval::count();
				
		$WorkingHoursInterval = new WorkingHoursInterval;
        $WorkingHoursInterval->working_hours_global_id      = 0;
       
        // SAVE WorkingHoursInterval
        $WorkingHoursInterval->save();
        $WorkingHoursIntervalID = $WorkingHoursInterval->id;
		
		$this->assertEquals($WorkingHoursIntervalCount+1, WorkingHoursInterval::count());
		$this->assertEquals(0, $WorkingHoursInterval->working_hours_global_id);
		
		
		//Update test 
		$WorkingHoursInterval          = WorkingHoursInterval::find($WorkingHoursIntervalID);
        $WorkingHoursInterval->working_hours_global_id 	= 1;
        $WorkingHoursInterval->save();
		$this->assertEquals(1, $WorkingHoursInterval->working_hours_global_id);
		
		//Removal test
		$this->assertEquals($WorkingHoursIntervalCount+1, WorkingHoursInterval::count());
		$WorkingHoursInterval = WorkingHoursInterval::find($WorkingHoursIntervalID);
		$WorkingHoursInterval->delete();
		$this->assertEquals($WorkingHoursIntervalCount, WorkingHoursInterval::count());
		
    }


	
}
