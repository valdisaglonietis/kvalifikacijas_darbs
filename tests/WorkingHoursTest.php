<?php

use App\Models\WorkingHours;

class WorkingHoursTest extends TestCase
{
	
	public function testWorkingHoursModel(){
      	
      	$WorkingHoursCount = WorkingHours::count();
				
		$WorkingHours = new WorkingHours;
        $WorkingHours->status      = 0;
       
        // SAVE WorkingHours
        $WorkingHours->save();
        $WorkingHoursID = $WorkingHours->id;
		
		$this->assertEquals($WorkingHoursCount+1, WorkingHours::count());
		$this->assertEquals(0, $WorkingHours->status);
		
		
		//Update test 
		$WorkingHours          = WorkingHours::find($WorkingHoursID);
        $WorkingHours->status	= 1;
        $WorkingHours->save();
		$this->assertEquals(1, $WorkingHours->status);
		
		//Removal test
		$this->assertEquals($WorkingHoursCount+1, WorkingHours::count());
		$WorkingHours = WorkingHours::find($WorkingHoursID);
		$WorkingHours->delete();
		$this->assertEquals($WorkingHoursCount, WorkingHours::count());
		
    }


	
}
