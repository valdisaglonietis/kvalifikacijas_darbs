<?php

use App\Models\WorkingHoursGlobal;

class WorkingHoursGlobalTest extends TestCase
{
	
	public function testWorkingHoursGlobalModel(){
      	
      	$WorkingHoursGlobalCount = WorkingHoursGlobal::count();
				
		$WorkingHoursGlobal = new WorkingHoursGlobal;
        $WorkingHoursGlobal->user_id      = 0;
       
        // SAVE WorkingHoursGlobal
        $WorkingHoursGlobal->save();
        $WorkingHoursGlobalID = $WorkingHoursGlobal->id;
		
		$this->assertEquals($WorkingHoursGlobalCount+1, WorkingHoursGlobal::count());
		$this->assertEquals(0, $WorkingHoursGlobal->user_id);
		
		
		//Update test 
		$WorkingHoursGlobal          = WorkingHoursGlobal::find($WorkingHoursGlobalID);
        $WorkingHoursGlobal->user_id 	= 1;
        $WorkingHoursGlobal->save();
		$this->assertEquals(1, $WorkingHoursGlobal->user_id);
		
		//Removal test
		$this->assertEquals($WorkingHoursGlobalCount+1, WorkingHoursGlobal::count());
		$WorkingHoursGlobal = WorkingHoursGlobal::find($WorkingHoursGlobalID);
		$WorkingHoursGlobal->delete();
		$this->assertEquals($WorkingHoursGlobalCount, WorkingHoursGlobal::count());
		
    }


	
}
