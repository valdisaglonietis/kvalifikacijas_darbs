<?php

use App\Models\Patients;
use App\Models\PatientActivity;


class PatientsTest extends TestCase
{
	
	public function testPatientsModel(){
      	
      	$PatientsCount = Patients::count();
				
		$Patients = new Patients;
        $Patients->first_name      = 'Test Paitent';
       
        // SAVE Patients
        $Patients->save();
        $PatientsID = $Patients->id;
		
		$this->assertEquals($PatientsCount+1, Patients::count());
		$this->assertEquals('Test Paitent', $Patients->first_name);
		
		
		//Update test 
		$Patients          = Patients::find($PatientsID);
        $Patients->first_name 	= 'Test Paitent 2';
        $Patients->save();
		$this->assertEquals('Test Paitent 2', $Patients->first_name);
		
		//Removal test
		$this->assertEquals($PatientsCount+1, Patients::count());
		$Patients = Patients::find($PatientsID);
		$Patients->delete();
		$this->assertEquals($PatientsCount, Patients::count());
		
    }
    
    public function testPatientsActivity(){
	    $userCount  = PatientActivity::count();
		$list 		= PatientActivity::all();
		$this->assertEquals($userCount, PatientActivity::count());
    }


	
}
