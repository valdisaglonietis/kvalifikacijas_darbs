<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable, Authorizable, CanResetPassword;
    use HasRoles, SoftDeletes;

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function getFullNameAttribute() {
		return "$this->title $this->first_name $this->last_name";
	}

	public function group(){
		return $this->belongsTo('App\Models\UserGroup', 'user_group_id');
	}
	
	
	
	public function findByName($name){
		return self::whereFirst_name($name)->get();
	}

	public function logs(){
		return $this->hasMany('App\Models\UserActivities', 'userid');
	}


	public function hidden_access_groups(){
		//return $this->has_many_and_belongs_to('HiddenAccessGroup', 'HiddenAccessGroup_User');
		return $this->hasManyThrough('App\Models\HiddenAccessGroup', 'Models\HiddenAccessGroup_User');
	}

	public function getRememberToken(){
		return null; // not supported
	}

	public function setRememberToken($value){
		// not supported
	}

	public function getRememberTokenName(){
		return null; // not supported
	}

	/**
	* Overrides the method to ignore the remember token.
	*/
	public function setAttribute($key, $value){
		$isRememberTokenAttribute = $key == $this->getRememberTokenName();
		if (!$isRememberTokenAttribute){
		  parent::setAttribute($key, $value);
		}
	}

	public function workingHours(){
		return $this->hasMany('App\Models\WorkingHours', 'user_id');
	}
}
