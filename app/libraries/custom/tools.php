<?php

function __transl($id,$key,$default,$locate){

    $appLang = Config::get('application.languages');

    if($appLang[0] != $locate){

        $translations = Translations::getTranslations($id,$locate);
        if(isset($translations[$key]) && !empty($translations[$key])){
            return $translations[$key];
        }

    }
    return $default;
}

function createPermission($module,$action){
    $newPermissionForAll = new Permission;
    $newPermissionForAll->name = $module.'_'.$action;
    $newPermissionForAll->module = $module;
    $newPermissionForAll->action = $action;
    $newPermissionForAll->guard_name = 'web';
    $newPermissionForAll->save();
}

function hasPermissions($module,$action,$returnView = true){
    $permission = Permission::where('name',$module.'_'.$action)->first();
    if(isset($permission) && $permission){
        if(Auth::user()->hasPermissionTo($permission->id)) return true;
    }else{
        createPermission($module,$action);
    }

    $permissionForAll = Permission::where('name',$module.'_all')->first();
    if(isset($permissionForAll) && $permissionForAll){
        if(Auth::user()->hasPermissionTo($permissionForAll->id))return true;
    }else{
        createPermission($module,'all');
    }

    if(Auth::user()->super_admin) return true;

    if($returnView) {
        echo getError(403);
        exit;
    }else{
        return false;
    }
}

function getError($error){
    return View::make('admin.error.index',['error' => $error]);
}

function getRealIpAddress(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        return $_SERVER['REMOTE_ADDR'];
    }
}

function percentage($val1, $val2, $precision=2) {
	$division = $val1 / $val2;
	$res = $division * 100;
	$res = round($res, $precision);
	return $res;
}

function getAge($date){
	$timeZone  = new DateTimeZone('Europe/Riga');
	$date = new DateTime($date, $timeZone);
	$age = $date->diff(new DateTime('now', $timeZone));
	return $age->y;
}

function _nr($int){
	return number_format(floatval($int),2,'.','');
}

function formatDate($date,$lang,$format='d.m.Y'){

	$translationsPregMatch['en'] = array('/Monday/' , '/Tuesday/', '/Wednesday/', '/Thursday/' , '/Friday/'  , '/Saturday/', '/Sunday/' , '/January/', '/February/', '/March/', '/April/', '/May/', '/June/', '/July/', '/August/', '/September/', '/October/', '/November/', '/December/');
	$translations['lv'] = array('pirmdiena', 'otrdiena' , 'trešdiena'  , 'ceturtdiena', 'piektdiena', 'sestdiena' , 'svētdiena', 'janvāris' , 'februāris' , 'marts'  , 'aprīlis', 'maijs', 'jūnijs', 'jūlijs', 'augusts' , 'septembris' , 'oktobris' , 'novembris' , 'decembris' );

	$date = date($format, strtotime($date));

	if($lang && $lang != 'en' && array_key_exists($lang,$translations)){
        return preg_replace($translationsPregMatch['en'], $translations[$lang], $date);
    }

	return $date;
}

function convertDate($date, $format = 'd.m.Y') {
  $date = strtotime($date);
  return date($format, $date);
}

function foratAndTestDate($date){
	return date('d.m.Y H:i',strtotime($date));
}

function generateGDPRCode($length = 16) {
    $characters = 'ABCDEFGHIJKLMNOPRSTUVZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getUniqueGDPRCode($model, $field) {
    $model = app('App\\Models\\'.$model);
    $code = generateGDPRCode();
    while($model::where($field,$code)->count()){
        $code = generateGDPRCode();
    }
    return $code;
}

function saveFile($fileName,$fileLocation,$oldFileName = '') {
    $allowedExtensions = [
        'jpg',
        'jpeg',
        'gif',
        'svg',
        'png'
    ];
    $file_ext = Input::file($fileName)->getClientOriginalExtension();
    if( in_array(strtolower($file_ext), $allowedExtensions)){
        if (!is_dir(public_path() . $fileLocation)){
            $directory = public_path();
            foreach(explode('/',$fileLocation) as $photoFolder){
                if(strlen($photoFolder) >= 1){
                    $directory .='/'.$photoFolder;
                    if (!is_dir($directory)){
                        mkdir($directory);
                    }
                }
            }
        }
        $newFileName = \Str::random(32);

        Input::file($fileName)->move(public_path().$fileLocation, $newFileName.'.'.strtolower($file_ext));
        return $newFileName.'.'.strtolower($file_ext);
    }
    return $oldFileName;
}
