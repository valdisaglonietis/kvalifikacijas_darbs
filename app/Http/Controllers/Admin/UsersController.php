<?php namespace App\Http\Controllers\Admin;
use View;
use Hash;
use Lang;
use Auth;
use Input;
use Config;
use Session;
use Redirect;
use App\User;
use Validator;
use App\Access;
use App\Models\UserActivities;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Routing\Controller as BaseController;
use DB;

class UsersController extends BaseController{
    protected $url = '/users/';
    protected $view = 'admin.users.';
    protected $title = 'users';
    protected $photoLocation = '/media/user_images/';

    /**
     * Returns view with datatable
     * 
     * Returns view which contains a datatable. Datatable send an ajax request to getList() function 
     * and list of objects are returned and displayed
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'url'         => $this->url,
            'title'       => $this->title,
        ]);
	}

    /**
     * Returns json encoded list of objects
     * 
     * Returns json encoded list of objects. 
     * Is called by datatable ajax request from getIndex() view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @return string
     */
    public function getList() {
	    // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // FIND ALL USERS
        $users = User::select('Users.*');
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')){
            $datatable = Input::get('datatable');
        }
        if(isset($datatable) && isset($datatable['sort'])){
                $users = $users->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }else{
            $users = $users->orderBy('Users.id','desc');
        }
        if(Input::has('query') && strlen(Input::get('query')['generalSearch']) > 0){
            $searchQuery = Input::get('query')['generalSearch'];
        }
        // IF SELECT2 QUERY
        if(Input::has('_type')){
            $searchQuery = Input::get('term','');
            $users = $users->addSelect(DB::raw("CONCAT(Users.first_name,' ',Users.last_name) as text"));
        }
        // SEARCH QUERY
        if(isset($searchQuery)){
            $users = $users
                ->join('model_has_roles', function ($join) {
                    $join->on('Users.id', '=', 'model_has_roles.model_id')
                        ->where('model_has_roles.model_type', '=', 'App\User');
                })
                ->join('roles', function ($join) {
                    $join->on('roles.id', '=', 'model_has_roles.role_id');
                })
                ->Where('Users.id', 'LIKE', "%$searchQuery%")
                ->orWhere('Users.first_name', 'LIKE', "%$searchQuery%")
                ->orWhere('Users.last_name','LIKE',"%$searchQuery%")
                ->orWhere('Users.email','LIKE',"%$searchQuery%")
                ->orWhere('Users.created_at','LIKE',"%$searchQuery%")
                ->orWhere('roles.key','LIKE',"%$searchQuery%")
                ->orWhere(DB::raw("CONCAT(Users.first_name,' ',Users.last_name)"),'LIKE',"%$searchQuery%");
        }
        // EXECUTE QUERY
        $users = $users->get();
        // MODIFY DATA
        foreach ($users as $user){
            $user->asignedRoles = $user->roles->pluck('key')->toArray();
        }
        // RETURN ENCODED USERS
        return json_encode($users);
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd(){
        // CHECK PERMISSSIONS
        hasPermissions($this->title,'edit');
        // GET ALL USERROLES
        $userRoles = Role::all();
        // RETURN VIEW
        return View::make($this->view.'add',[
            'url' => $this->url,
            'title' => $this->title,
            'view' => $this->view,
            'user_roles' =>$userRoles,
        ]);
    }

    /**
     * Creates a new object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object is created, filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'email'    => 'required|email|unique:Users,email,NULL,id,deleted_at,NULL|max:191',
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'password' => 'required|confirmed|max:191',
            'userroles' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // CREATE USER
        $user = new User;
        // INSERT DATA
        $user->email      = Input::get('email','');
        $user->first_name = Input::get('first_name','');
        $user->last_name  = Input::get('last_name','');
        $user->phone      = Input::get('phone','');
        $user->username   = Input::get('email','');

        if(Input::file('photo')) {
            $user->photo = saveFile('photo',$this->photoLocation);
        }

        if(Input::get('password')){
            $user->password = Hash::make(Input::get('password'));
        }

        if(Auth::user()->super_admin){
            $user->active = Input::has('active');
        }
        // SAVE USER
        $user->save();
        // SYNC ROLES
        $user->syncRoles(Input::get('userroles') ? : []);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'created', $user->id,$user->full_name);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }


    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 3 buttons. "Submit" , that sends a POST request to postEdit() function to save the edited object,
     * "Cancel" , that redirects user to getIndex() view ,and "Delete", that sends a get request to getDelete()
     * function and deletes the object
     *
     * @param $id
     * @return mixed
     */
    public function getEdit($id){
	    // CHECK PERMISSSIONS
        hasPermissions($this->title,'edit');
        // FIND USER
        $user = User::findOrFail($id);
        // GET ALL USER ROLES
        $userRoles = Role::all();
        // RETURN VIEW
        return View::make($this->view.'edit',[
            'url' => $this->url,
            'title' => $this->title,
            'view' => $this->view,
            'user' => $user,
            'user_roles' =>$userRoles,
            'photoLocation' => $this->photoLocation
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        // CHECK PERMISSIONS
	    hasPermissions($this->title,'edit');
        // FIND USER
        $user = User::findOrFail($id);
        // VALIDATE
        $rules = [
            'email'    => 'required|email|unique:Users,email,'.$id.',id,deleted_at,NULL|max:191',
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'password' => 'confirmed|max:191',
            'userroles' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // INSERT DATA
        $user->email      = Input::get('email','');
        $user->first_name = Input::get('first_name','');
        $user->last_name  = Input::get('last_name','');
        $user->phone      = Input::get('phone','');
        $user->username   = Input::get('email');

        if(Input::file('photo')) {
            $oldPhoto = $user->photo;
            $user->photo = saveFile('photo',$this->photoLocation,$user->photo);
            if($user->photo != $oldPhoto){
                unlink(public_path().$this->photoLocation.$oldPhoto);
            }
        }

        if(Input::get('password')){
            $user->password = Hash::make(Input::get('password'));
        }

        if(Auth::user()->super_admin){
            $user->active = Input::has('active');
        }
        // SAVE USER
        $user->save();
        // SYNC ROLES
        $user->syncRoles(Input::get('userroles') ? : []);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $user->id,$user->full_name);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     * Deletes an existing object and returns a redirect to getIndex() function
     *
     * Deletes an existing object if it is found. Logs succesful attempts. After successful delete returns a redirect
     * to getIndex() function. Can be called from getEdit() function view by a button "Delete"
     * 
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
	{
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND USER
        $user = User::findOrFail($id);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'deleted', $user->id,$user->full_name);
        if($user->image){
            $user->password = Hash::make(Input::get('password'));
        }
        // DELETE USER
        $user->delete();
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        // REDIRECT TO LIST
		return Redirect::to($this->url);
	}


    /**
     * returns a json encoded list of UserActivities objects
     *
     * Returns json encoded list of objects.
     * Is called by datatable ajax request from getEdit() functions view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @param $id
     * @return string
     */
    public function getLogs($id){
	    // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND ACTIVITIES
        $data = UserActivities::where('userid',$id);
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')) {
            $dataTable = Input::get('datatable');
        }
        if(isset($dataTable) && isset($dataTable['sort'])) {
            $data = $data->orderBy($dataTable['sort']['field'],$dataTable['sort']['sort']);
        } else {
            $data = $data->orderBy('created_at','desc');
        }
        // SEARCH QUERY
        if(Input::has('query') && strlen(Input::get('query')['userLogs_search']) > 2) {
            $searchQuery = Input::get('query')['userLogs_search'];
            $data = $data->where('action','LIKE',"%$searchQuery%")
                ->orWhere('module','LIKE',"%$searchQuery%")
                ->orWhere('created_at','LIKE',"%$searchQuery%")
                ->orWhere('extra_text','LIKE',"%$searchQuery%")
                ->orWhere('extra_id','LIKE',"%$searchQuery%");
        }
        $data = $data->select('created_at','action','module','extra_id','extra_text');
        // EXECUTE QUERY AND RETURN ENCODED LOGS
        return json_encode($data->get());
    }

    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 1 button: "Submit" , that sends a POST request to postProfile() function to save the edited object
     *
     * @return mixed
     */
    public function getProfile(){
        // FIND USER
        $user = User::findOrFail(Auth::user()->id);
        // GET ALL USER ROLES
        $userRoles = Role::all();
        // RETURN VIEW
        return View::make($this->view.'profile',[
            'url' => $this->url,
            'title' => $this->title,
            'view' => $this->view,
            'user' => $user,
            'user_roles' =>$userRoles,
            'photoLocation' => $this->photoLocation
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getProfile() function
     *
     * @return mixed
     */
    public function postProfile() {
// CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND USER
        $user = User::findOrFail(Auth::user()->id);

        if(Input::file('photo')) {
            $oldPhoto = $user->photo;
            $user->photo = saveFile('photo',$this->photoLocation,$user->photo);
            if($user->photo != $oldPhoto){
                unlink(public_path().$this->photoLocation.$oldPhoto);
            }
        }

        if(Input::has('changeLocale') && in_array(Input::get('changeLocale'), Config::get('application.languages'))){
            $user->locale = Input::get('changeLocale');
            Cookie::queue('locale', $user->locale, 2147483647,'/');
        }

        $user->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $user->id,$user->full_name);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url.'profile');
    }
}