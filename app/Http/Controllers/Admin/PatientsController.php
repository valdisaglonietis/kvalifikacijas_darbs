<?php namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use Auth;
use App\Models\Patients;
use Validator;
use DB;
use App\Models\WorkingHours;

class PatientsController extends BaseController{
    protected $url = '/patients/';
    protected $view = 'admin.patients.';
    protected $title = 'patients';

    /**
     * Returns view with datatable
     *
     * Returns view which contains a datatable. Datatable send an ajax request to getList() function
     * and list of objects are returned and displayed
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'url'         => $this->url,
            'title'       => $this->title,
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by datatable ajax request from getIndex() view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @return string
     */
    public function getList()
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // FIND ALL USERS
        $Patients = Patients::select('Patients.*');
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')){
            $datatable = Input::get('datatable');
        }
        if(isset($datatable) && isset($datatable['sort'])){
            $Patients = $Patients->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }else{
            $Patients = $Patients->orderBy('Patients.id','desc');
        }
        if(Input::has('query') && strlen(Input::get('query')['generalSearch']) > 0){
            $searchQuery = Input::get('query')['generalSearch'];
        }
        // IF SELECT2 QUERY
        if(Input::has('_type')){
            $searchQuery = Input::get('term','');
            $Patients = $Patients->addSelect(DB::raw("CONCAT(Patients.first_name,' ',Patients.last_name) as text"));
        }
        // SEARCH QUERY
        if(isset($searchQuery)){
            $Patients = $Patients
                ->Where('Patients.id', 'LIKE', "%$searchQuery%")
                ->orWhere(DB::raw("CONCAT(Patients.first_name,' ',Patients.last_name)"), 'LIKE', "%$searchQuery%")
                ->orWhere('Patients.email','LIKE',"%$searchQuery%")
                ->orWhere('Patients.phone','LIKE',"%$searchQuery%");
        }
        // EXECUTE QUERY AND RETURN ENCODED USERS
        return json_encode($Patients->get());
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // RETURN VIEW
        return View::make($this->view.'add',[
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     * Creates a new object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object is created, filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'first-name' => 'required|max:191',
            'last-name' => 'required|max:191',
            'phone' => 'required|max:191',
            'email' => 'nullable|email|max:191',
            'sex' => 'sometimes|numeric|max:11'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // CREATE NEW ROOM
        $patient         = new Patients;
        // INSERT DATA
        $patient->first_name = Input::get('first-name','');
        $patient->last_name = Input::get('last-name','');
        $patient->phone = Input::get('phone','');
        $patient->email = Input::get('email','');
        $patient->sex = Input::get('sex',0);
        // SAVE NEW ROOM
        $patient->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'added', $patient->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 3 buttons. "Submit" , that sends a POST request to postEdit() function to save the edited object,
     * "Cancel" , that redirects user to getIndex() view ,and "Delete", that sends a get request to getDelete()
     * function and deletes the object
     *
     * @param $id
     * @return mixed
     */
    public function getEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND ROOM
        $patient = Patients::findOrFail($id);
        // RETURN VIEW
        return View::make($this->view.'edit',[
            'patient'  => $patient,
            'title' => $this->title,
            'url' => $this->url,
            'view' => $this->view
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'first-name' => 'required|max:191',
            'last-name' => 'required|max:191',
            'phone' => 'required|max:191',
            'email' => 'nullable|email|max:191',
            'sex' => 'sometimes|numeric|max:11'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // CREATE NEW ROOM
        $patient         = Patients::findOrFail($id);
        // INSERT DATA
        $patient->first_name = Input::get('first-name','');
        $patient->last_name = Input::get('last-name','');
        $patient->phone = Input::get('phone','');
        $patient->email = Input::get('email','');
        $patient->sex = Input::get('sex',0);
        // SAVE NEW ROOM
        $patient->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $patient->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     * Deletes an existing object and returns a redirect to getIndex() function
     *
     * Deletes an existing object if it is found. Logs succesful attempts. After successful delete returns a redirect
     * to getIndex() function. Can be called from getEdit() function view by a button "Delete"
     *
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND ROOM
        $patient = Patients::findOrFail($id);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'deleted', $patient->id);
        // DELETE PATIENT
        $patient->delete();
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    public function getHealthHistory($id) {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // SET DATABASE LANGUAGE
        DB::select("SET lc_time_names = 'lv_LV';");
        // FIND ALL USERS
        $workingHours = WorkingHours::select(
            'WorkingHours.*',
            DB::raw('CONCAT(Users.first_name," ",Users.last_name) as doctor'),
            DB::raw("CONCAT(Rooms.name,' (',Rooms.room_number,')') as room"))
            ->where('WorkingHours.patient_id',$id)
            ->leftJoin('WorkingHoursGlobal','WorkingHours.working_hours_global_id','=','WorkingHoursGlobal.id')
            ->leftJoin('Users', 'WorkingHoursGlobal.user_id', '=', 'Users.id')
            ->leftJoin('Rooms', 'WorkingHoursGlobal.room_id', '=', 'Rooms.id');
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')){
            $datatable = Input::get('datatable');
        }
        if(isset($datatable) && isset($datatable['sort'])){
            $workingHours = $workingHours->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }else{
            $workingHours = $workingHours->orderBy('WorkingHours.id','desc');
        }
        if(Input::has('query') && strlen(Input::get('query')['healthHistorySearch']) > 0){
            $searchQuery = Input::get('query')['healthHistorySearch'];
        }
        // SEARCH QUERY
        if(isset($searchQuery)){
            $workingHours = $workingHours->where(function ($query) use ($searchQuery) {
                $query->Where('WorkingHours.id', 'LIKE', "%$searchQuery%")
                    ->orWhere(DB::raw("CONCAT(Users.first_name,' ',Users.last_name)"), 'LIKE', "%$searchQuery%")
                    ->orWhere(DB::raw("CONCAT(Rooms.name,' (',Rooms.room_number,')')"), 'LIKE', "%$searchQuery%")
                    ->orWhere(DB::raw('CONCAT(DATE_FORMAT(WorkingHours.start, "%W, %d. %M %Y %H:%i"), " - ", DATE_FORMAT(WorkingHours.end, "%W, %d. %M %Y %H:%i"))'),'LIKE',"%$searchQuery%");
            });
        }
        // EXECUTE QUERY
        $workingHours = $workingHours->get();
        // MODIFY DATA
        foreach($workingHours as $key => $workingHour){
            $workingHours[$key]->start = formatDate($workingHour->start,'lv','l, d. F Y H:i') . ' - ' . formatDate($workingHour->end,'lv','H:i');
        }

        // EXECUTE QUERY AND RETURN ENCODED USERS
        return json_encode($workingHours);
    }
}