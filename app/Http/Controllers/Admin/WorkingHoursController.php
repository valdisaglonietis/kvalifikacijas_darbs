<?php namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use App\Models\UsersFilialesJoins;
use Auth;
use App\Models\Patients;
use App\Models\WorkingHours;
use App\Models\WorkingHoursGlobal;
use DateTime;
use App\Models\ManipulationsGroups;
use App\Models\Manipulations;
use App\Models\Rooms;
use App\Models\Investigations;
use App\Models\Services;
use App\Models\UsersToManipulations;
use DB;
use App\Models\WorkingHoursInterval;
use Config;

class WorkingHoursController extends BaseController
{
    protected $url = '/working-hours/';
    protected $view = 'admin.working-hours.';
    protected $title = 'working-hours';

    /**
     * Returns view with fullcalendar
     *
     * Returns view which contains a fullcalendar. Fullcalendar send an ajax request to getList() function
     * and list of events are returned and displayed. Fullcalendar also sends an ajax request to 'rooms/list' to
     * get the list of all active rooms, that events are sorted by. When click on events user is redirected to
     * getEdit() function with specific ID. When click on button "Add new", user is redirected to
     * getAdd() function without ID.
     *
     * @return mixed
     */
    public function getIndex()
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view . 'index', [
            'url' => $this->url,
            'title' => $this->title,
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by fullcalendar ajax request from getIndex() view.
     * Checks additional params from fullcalendar ajax request and queries the results
     *
     * @return string
     */
    public function getList()
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // CHECK REQUIRED PARAMS
        if(!Input::has('start')||!Input::has('end')){
            return json_encode([]);
        }
        $start = date('Y-m-d 00:00:00',strtotime(Input::get('start','')));
        $end = date('Y-m-d 23:59:59', strtotime(Input::get('end','')));
        // FIND ALL WORKING HOURS IN INTERVAL
        $workingHours = WorkingHoursInterval::where('WorkingHoursInterval.start', '>=', $start)
            ->where('WorkingHoursInterval.end', '<=', $end);
        // JOIN CONNECTED OBJECTS
        $workingHours = $workingHours->leftJoin('WorkingHoursGlobal', 'WorkingHoursInterval.working_hours_global_id', '=', 'WorkingHoursGlobal.id')
        ->leftJoin('Users', 'WorkingHoursGlobal.user_id', '=', 'Users.id')
        ->leftJoin('Rooms', 'WorkingHoursGlobal.room_id', '=', 'Rooms.id');
        // SELECT AND EXECUTE QUERY
        $workingHours = $workingHours->select(
            'WorkingHoursInterval.id as id',
            'WorkingHoursInterval.start as start',
            'WorkingHoursInterval.end as end',
            DB::raw("CONCAT(Users.first_name,' ',Users.last_name) as title"),
            'Rooms.id as resourceId'
        )->get();
        // MODIFY DATA
        foreach ($workingHours as $workingHour) {
            $workingHour->start_date = date('Y-m-d', strtotime($workingHour->start));
            $workingHour->start_time = date('H:i', strtotime($workingHour->start));
            $workingHour->end_time = date('H:i', strtotime($workingHour->end));
            $workingHour->url = $this->url.'edit/'.$workingHour->id;
        }
        // RETURN JSON ENCODED WORKING HOURS
        return json_encode($workingHours);
    }

    /**
     * Returns a view with a form to add a new object or edit if $id is present
     *
     * Returns a view with a form to add a new object or edit if $id is present. The view displays one or more fields
     * to fill and has 2 buttons. "Submit" , that sends a POST request to postEdit() function to create a new object
     * or edit an existing one if $id is present and "Cancel" that redirects user to getIndex() view.
     *
     * @return mixed
     */
    public function getEdit($id = null)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // GET INTERVAL HOURS
        $intervalHours = self::getWorkingHoursInterval($id);
        // GET GLOBAL HOURS
        $globalHours = self::getWorkingHoursGlobal($intervalHours->working_hours_global_id);
        // RETURN VIEW
        return View::make($this->view . 'edit', [
            'title' => $this->title,
            'url' => $this->url,
            'intervalHours' => $intervalHours,
            'globalHours' => $globalHours,
            'weekdays' => explode(',', $intervalHours->day_numbers),
            'weekDayNames' => Config::get('working-hours.weekdays'),
            'daysTimeFrom' => explode(',', $globalHours->days_time_from),
            'daysTimeTo' => explode(',', $globalHours->days_time_to),
            'defaultDuration' => Config::get('working-hours.duration')
        ]);
    }

    /**
     *
     * Creates a new object or edits an existing object if $id is present and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object or an existing object if $id is present is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function. First function checks if the input data is valid. Then it is checked if anything will be created and
     * after all reality overlapping posibilties are excluded(2 person in the same room or 1 person in 2 rooms at the same time). If object is an
     * existing one, then already asigned hours are checked. If there are any, then the work hours can't be edited.
     *
     * @param $id
     * @return mixed
     */

    public function postEdit($id = null)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $validator = \Validator::make(\Input::all(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'doctor' => 'required|exists:Users,id',
            'room' => 'required|exists:Rooms,id',
            'weekday' => 'required|Array',
            'time_from' => 'required|Array|size:7',
            'time_to' => 'required|Array|size:7'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }
        // CUSTOM VALIDATION
        $errors = [];
        $date_from = date('Y-m-d', strtotime(Input::get('date_from')));
        $date_to = date('Y-m-d', strtotime(Input::get('date_to')));
        // CHECK THAT START DATE IS SMALLER THAN END TIME
        if ($date_from > $date_to) {
            $validator->getMessageBag()->add('duration', Lang::get('admin.required-interval-start-smaller-than-end'));
            $errors[] = true;
        }
        // CHECK THAT START TIME IS SMALLER THAN END TIME
        foreach (array_keys(Input::get('weekday')) as $day_checked) {
            if ((strlen(Input::get('time_from')[$day_checked]) >= strlen(Input::get('time_to')[$day_checked])) && Input::get('time_from')[$day_checked] >= Input::get('time_to')[$day_checked] && !count($errors)) {
                $validator->getMessageBag()->add('duration', Lang::get('admin.required-start-time-smaller-than-end-time'));
                $errors[] = true;
            }
        }
        // REDIRECT BACK IF ANY ERRORS
        if (count($errors)) {
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }
        // SET VARIABLES
        $weekday = array_keys(Input::get('weekday'));
        $temp_date_from = $date_from;
        $temp_date_to = $date_to;
        $duration = Config::get('working-hours.duration');
        $i = 0;
        // CHECKS IF ANY DAY WOULD BE CREATED
        while ($temp_date_from <= $temp_date_to) {
            $dayofweek = date('N', strtotime($temp_date_from)) - 1;
            echo $dayofweek;
            if (in_array($dayofweek, $weekday) || $i > 365) {
                $date_exists = true;
                break;
            }
            $i++;
            $temp_date_from = date('Y-m-d', strtotime($temp_date_from . "+1 days"));
        }
        if (!(isset($date_exists))) {
            $validator->getMessageBag()->add('weekday', Lang::get('admin.chosen-period-will-not-create-anything'));
            return redirect()->back()->withErrors($validator)->withInput(Input::all());
        }
        // SET VARIABLES
        $user = User::find(Input::get('doctor'));
        $room = Rooms::find(Input::get('room'));
        $time_from = Input::get('time_from');
        $time_to = Input::get('time_to');
        $temp_date_from = date('Y-m-d', strtotime($date_from)) . ' 00:00:00';
        $temp_date_to = date('Y-m-d', strtotime($date_to)) . ' 23:59:59';
        $globalHours = self::getWorkingHoursGlobalFromIntervalId($id);
        $noInspectionsAsigned = 0;
        $overlappingGlobalHoursCount = 0;
        // VALIDATE EDIT
        // CHECK IF ANY GLOBAL HOURS WITH USER OR ROOM OVERLAP(user at 2 rooms, or 2 people at the same room)
        $overlappingGlobalHoursCount = WorkingHoursGlobal::whereNull('WorkingHoursGlobal.deleted_at')
            ->where('WorkingHoursGlobal.start', '<=', $temp_date_to)
            ->where('WorkingHoursGlobal.end', '>=', $temp_date_from)
            ->where('WorkingHoursGlobal.id','!=',$globalHours->id)
            ->where(function ($query) use ($user, $room) {
                $query->where('WorkingHoursGlobal.user_id', $user->id)->orWhere('WorkingHoursGlobal.room_id', $room->id);
            })
            ->where(function ($query) use ($weekday) {
                foreach ($weekday as $day) {
                    $query->orWhere('WorkingHoursGlobal.day_numbers', 'like', "%$day%");
                }
            })
            ->count();
        if ($globalHours->id) {
            // CHECK IF ANY INVESTIGATIONS ARE ASSIGNED
            $alreadyAsignedWorkingHours = WorkingHours::where('working_hours_global_id', $globalHours->id)
                ->where('investigation_id', '!=', null)->whereNull('deleted_at')->get();
            $noInspectionsAsigned = count($alreadyAsignedWorkingHours);
        }
        // CREATE GLOBAL HOUR
        if (!$overlappingGlobalHoursCount && !$noInspectionsAsigned) {
            // INSERT DATA
            $globalHours->description =
                Lang::get('admin.worker') . ': <span>' . $user->full_name . "</span><br>" .
            $globalHours->start = $temp_date_from;
            $globalHours->end = $temp_date_to;
            $globalHours->user_id = $user->id;
            $globalHours->room_id = $room->id;
            $globalHours->day_numbers = implode(',', $weekday);
            $globalHours->days_time_from = implode(',',$time_from);
            $globalHours->days_time_to = implode(',', $time_to);
            $globalHours->duration = $duration;
            // SAVE GLOBAL HOUR
            $globalHours->save();
            // DELETE PREVIOUS RECORDS
            WorkingHoursInterval::where('working_hours_global_id',$globalHours->id)->delete();
            WorkingHours::where('working_hours_global_id',$globalHours->id)->delete();
            // SET VARIABLES
            $dayCount = 0;
            $maxWorkHoursIntervalCount = Config::get('working-hours.maxWorkHoursIntervalCount');
            $maxWorkHoursCount = $maxWorkHoursIntervalCount*Config::get('working-hours.maxWorkHoursCount');
            // CREATE INTERVAL HOURS
            while ($date_from <= $date_to) {
                // SET VARIABLES
                $dayofweek = date('N', strtotime($date_from))-1;
                // CHECK IF SELECTED DAY
                if (in_array($dayofweek, $weekday)) {
                    // SET VARIABLES
                    $date_input_1 = $date_from . ' ' . Input::get('time_from')[$dayofweek] . ':00';
                    $date_input_2 = $date_from . ' ' . Input::get('time_to')[$dayofweek] . ':00';
                    // CREATE INTERVAL HOUR
                    $itemInterval = new WorkingHoursInterval();
                    // INSERT DATA
                    $itemInterval->working_hours_global_id = $globalHours->id;
                    $itemInterval->start = date('Y-m-d H:i:s', strtotime($date_input_1));//$start;
                    $itemInterval->end = date('Y-m-d H:i:s', strtotime($date_input_2));
                    // SAVE INTERVAL HOUR
                    $itemInterval->save();
                    // SET VARIABLES
                    $timeCount = 0;
                    // CREATE WORK HOURS
                    while (($current = strtotime('+' . $duration . ' minutes', strtotime($date_input_1))) <= strtotime($date_input_2)) {
                        // CREATE WORK HOUR
                        $item = new WorkingHours();
                        // INSERT DATA
                        $item->working_hours_global_id = $globalHours->id;
                        $item->start = date('Y-m-d H:i:s', strtotime($date_input_1));//$start;
                        $item->end = date('Y-m-d H:i:s', $current);
                        // SAVE WORK HOUR
                        $item->save();
                        // SET VARIABLES
                        $date_input_1 = date('Y-m-d H:i:s',$current);
                        $timeCount++;
                        // BREAK IF SOME ERROR
                        if($timeCount > $maxWorkHoursCount){
                            break;
                        }
                    }
                }
                // SET VARIABLES
                $date_from = date('Y-m-d', strtotime($date_from . "+1 days"));
                $dayCount++;
                // BREAK IF SOME ERROR
                if ($dayCount > $maxWorkHoursIntervalCount) {
                    break;
                }
            }
            // LOG ACTIVITIES AND OUTPUT MESSAGE
            if($id){
                UserActivities::registerV2($this->title,'edited', $globalHours->id,$user->full_name);
                Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
            }else{
                UserActivities::registerV2($this->title,'created', $globalHours->id,$user->full_name);
                Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
            }
            // RETURN VIEW
            return Redirect::to($this->url);
        } else {
            // OUTPUT ERRORS
            if($overlappingGlobalHoursCount){
                $errors = $errors[] = Lang::get('admin.workhours_overlap');
            }
            if (!$overlappingGlobalHoursCount && !$noInspectionsAsigned) {
                $errors = $errors[] = Lang::get('admin.workhours_have_investigations_asigned');
            }

        }
        // RETURN VIEW WITH INPUT AND ERRORS
        return Redirect::to($this->url . 'edit/'.$id)->withErrors($errors)->withInput(Input::all());
    }

    /**
     * Deletes an existing Work hours interval and returns a redirect to getIndex() function
     *
     * Deletes an existing Work hours interval if it is found and has no attached investigations. If attached
     * investigations are found, user is redirected back with error of existing investigation. Logs succesful
     * attempts. After successful delete returns a redirect to getIndex() function. Can be called from getEdit()
     * function view by a button "Delete". Deletes global_hours that is a parent of interval hours if no
     * more active interval hours exist that are asigned to specific global hours
     *
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND INTERVAL
        $Interval = WorkingHoursInterval::find($id);
        // FIND GLOBAL HOUR
        if(isset($Interval) && $Interval){
            $globalHours = WorkingHoursGlobal::find($Interval->working_hours_global_id);
        }
        // CHECK GLOBAL HOUR
        if(isset($globalHours) && $globalHours){
            // CHECK ASIGNED WORK HOURS
            $assignedWorkingHoursCount = WorkingHours::whereNull('deleted_at')
                ->where('working_hours_global_id', $globalHours->id)
                ->whereNotNull('investigation_id')
                ->where('start', '>=', $Interval->start)
                ->where('end', '<=', $Interval->end)
                ->count();
            // RETURN IF ASIGNED WORK HOURS EXIST
            if ($assignedWorkingHoursCount) {
                return redirect()->back()->withErrors([Lang::get('admin.investigation-is-in-interval-remote-it-before-trying-again')]);
            }
            // DELETE CONNECTED WORK HOURS
            $assignedWorkingHoursCount = WorkingHours::whereNull('deleted_at')
                ->where('working_hours_global_id', $globalHours->id)
                ->where('start', '>=', $Interval->start)
                ->where('end', '<=', $Interval->end)->delete();
            // DELETE INTERVAL
            $Interval->delete();
            // DELETE GLOBAL HOUR IF NO MORE INTERVALS EXIST
            $existingIntervalCount = WorkingHoursInterval::whereNull('deleted_at')
                ->where('working_hours_global_id',$globalHours->id)->count();
            if(!$existingIntervalCount) {
                // LOG ACTIVITIES
                UserActivities::registerV2($this->title,'deleted', $globalHours->id);
                // DELETE GLOBAL HOUR
                $globalHours->delete();
            }
            // LOG ACTIVITIES
            UserActivities::registerV2($this->title,'deleted', $globalHours->id,$Interval->id);
            // OUTPUT MESSAGE
            Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        }else{
            // ERROR
            Session::put('notify', ['message' => Lang::get('admin.error-please-contact-support'), 'type' => 'warning']);
        }
        // REDIRECT TO GETINDEX() VIEW
        return Redirect::to($this->url);
    }

    /**
     * Deletes an existing Global work hours and returns a redirect to getIndex() function
     *
     * Deletes an existing Global work hours  if it is found and has no attached investigations. If attached
     * investigations are found, user is redirected back with error of existing investigation. Logs succesful
     * attempts. After successful delete returns a redirect to getIndex() function. Can be called from getEdit()
     * function view by a button "Delete".
     *
     * @param $id
     * @return mixed
     */
    public function getDeleteAll($id)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND INTERVAL
        $interval = WorkingHoursInterval::find($id);
        // FIND GLOBAL HOUR
        if(isset($interval) && $interval){
            $globalHours = WorkingHoursGlobal::find($interval->working_hours_global_id);
        }
        // CHECK GLOBAL HOUR
        if(isset($globalHours) && $globalHours){
            // COUNT ASIGNED INVESTIGATIONS
            $asignedInvestigationsCount = WorkingHours::whereNull('deleted_at')
                ->where('working_hours_global_id', $globalHours->id)
                ->whereNotNull('investigation_id')
                ->count();
            // RETURN ALL ASIGNED INVESTIGATIONS IF THERE ARE
            if($asignedInvestigationsCount){
                $asignedInvestigations = WorkingHours::whereNull('deleted_at')
                    ->where('working_hours_global_id', $globalHours->id)
                    ->whereNotNull('investigation_id')
                    ->pluck('start');
                // REDIRECT WITH INFO OF ASIGNED INVESTIGATIONS
                return redirect()->back()->withErrors([
                    Lang::get('admin.investigation-is-in-interval-remote-it-before-trying-again').
                    Lang::get('admin.count').': '.$asignedInvestigationsCount,
                   Lang::get('admin.connected-investigations').': '. json_encode($asignedInvestigations)
                ]);
            }
            // DELETE CONNECTED WORKING HOURS
            WorkingHours::whereNull('deleted_at')
                ->where('working_hours_global_id', $globalHours->id)
                ->delete();
            // DELETE CONNECTED WORKING HOUR INTERVALS
            WorkingHoursInterval::whereNull('deleted_at')
                ->where('working_hours_global_id', $globalHours->id)
                ->delete();
            // LOG ACTIVITIES
            UserActivities::registerV2($this->title,'deleted', $globalHours->id);
            // OUTPUT MESSAGE
            Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
            // DELETE GLOBAL HOURS
            $globalHours->delete();
        }else{
            // ERROR
            Session::put('notify', ['message' => Lang::get('admin.error-please-contact-support'), 'type' => 'warning']);
        }
        // REDIRECT TO LIST VIEW
        return Redirect::to($this->url);
    }


    /**
     * Returns Working Hours Object depending on $id value
     *
     * Returns Working Hours Object depending on $id value
     *
     * @param null $id
     * @return WorkingHoursInterval
     */
    private function getWorkingHoursInterval($id = null) {
        // RETURN NEW IF NO ID
        if($id == null) {
            return new WorkingHoursInterval;
        }
        // RETURN FOUND INTERVAL HOURS
        return WorkingHoursInterval::findOrFail($id);
    }

    /**
     * Returns Global Working Hours Object depending on $id value
     *
     * Returns Global Working Hours Object depending on $id value
     *
     * @param null $id
     * @return WorkingHoursGlobal
     */
    private function getWorkingHoursGlobal($id = null) {
        // RETURN NEW IF NO ID
        if($id == null) {
            return new WorkingHoursGlobal;
        }
        // RETURNS FOUND GLOBAL HOURS
        return WorkingHoursGlobal::findOrFail($id);
    }

    /**
     * Returns Global Working Hours Object depending on Working Hours Interval $id value
     *
     * Returns Global Working Hours Object depending on Working Hours Interval $id value
     *
     * @param null $id
     * @return WorkingHoursGlobal
     */
    private function getWorkingHoursGlobalFromIntervalId($id = null) {
        // RETURN NEW IF NO ID
        if($id == null){
            return new WorkingHoursGlobal;
        }
        // FIND INTERVAL HOURS
        $workingHoursInterval = self::getWorkingHoursInterval($id);
        // RETURNS FOUND GLOBAL HOURS
        return WorkingHoursGlobal::findOrFail($workingHoursInterval->working_hours_global_id);
    }
}
