<?php namespace App\Http\Controllers\Admin;
use Auth;
use Input;
use View;
use Lang;
use Redirect;
use App\User;
use App\Http\Requests\Request;
use Illuminate\Routing\Controller as BaseController;

class LoginController extends BaseController {
    public $restful = true;
    public $profiler = true;
    protected $url = '/';
    protected $view = 'admin.login.';

    /**
     *  Returns view with with Login and Password recovery form
     *
     * Returns view with with Login and Password recovery form. Both forms are executed with ajax.
     * Login form ajax request calls the postLogin() function. If login request is successful user is redirected to
     * application dashboard. Otherwise a response about incorrect username or password is returned with status of 403.
     * Password recovery form ajax request calls postRequest() function from PasswordRecoveryController.
     * @return mixed
     */
    public function getLogin(){

        if(!Auth::check()){
            return View::make($this->view.'index')->with(array('title' => 'Login'));
        }
        return Redirect::to($this->url);
    }

    /**
     * Attempts to login the user into the system and
     * returns json encoded array with status and message about login attempt
     *
     * Attempts to login the user into the system and returns json encoded array with status and message about
     * login attempt. Status code is the type of response. If status is 200, then login attempt was succesful.
     * Otherwise status is 403 and error message is added.
     * 
     * @return string
     */
    public function postLogin(){
        // GET CREDENTIALS
        $credentials =[
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ];
        // ATTEMPT LOGIN
        if(Auth::attempt($credentials)){
            // CHECK USER STATUS
            if(!Auth::user()->active && Auth::user()->attempts < 7){
                Auth::logout();
            }else{
                // RESET FAILED LOGIN ATTEMPTS
                Auth::user()->attempts = 0;
                Auth::user()->save();
                // RETURN SUCCESFUL LOGIN
                return json_encode([
                    'status' => 200
                ]);
            }
        }
        // INCREASE FAILED LOGIN ATTEMPTS
        User::where('username',$credentials['username'])->limit(1)->increment('attempts');
        // RETURN FAILED LOGIN
        return json_encode([
            'status' => 403,
            'message' => Lang::get('admin.wrong_user_or_password')
        ]);
    }

    /**
     * Logs a user out of the system and redirects to the start page
     *
     * Logs a user out of the system and redirects to the start page. Can be called only if user is logged in.
     *
     * @return mixed
     */
    public function getLogout(){
        Auth::logout();
        return Redirect::to($this->url);
    }
}
