<?php namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use Auth;
use App\Models\Rooms;
use App\Models\ManipulationsGroups;
use Validator;

class RoomController extends BaseController{
    protected $url = '/rooms/';
    protected $view = 'admin.rooms.';
    protected $title = 'rooms';

    /**
     * Returns view with datatable
     *
     * Returns view which contains a datatable. Datatable send an ajax request to getList() function
     * and list of objects are returned and displayed
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'url'         => $this->url,
            'title'       => $this->title,
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by datatable ajax request from getIndex() view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @return string
     */
    public function getList() {

        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // FIND ALL USERS
        $Rooms = Rooms::select('Rooms.*');
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')){
            $datatable = Input::get('datatable');
        }
        if(isset($datatable) && isset($datatable['sort'])){
            $Rooms = $Rooms->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }else{
            $Rooms = $Rooms->orderBy('Rooms.id','desc');
        }
        if(Input::has('query') && strlen(Input::get('query')['generalSearch']) > 0){
            $searchQuery = Input::get('query')['generalSearch'];
        }
        // IF SELECT2 QUERY
        if(Input::has('_type')){
            $searchQuery = Input::get('term','');
            $Rooms = $Rooms->addSelect('Rooms.name as text');
        }

        // SEARCH QUERY
        if(isset($searchQuery)){
            $Rooms = $Rooms
                ->Where('Rooms.id', 'LIKE', "%$searchQuery%")
                ->orWhere('Rooms.name', 'LIKE', "%$searchQuery%")
                ->orWhere('Rooms.room_number','LIKE',"%$searchQuery%");
        }
        // EXECUTE QUERY AND RETURN ENCODED USERS
        return json_encode($Rooms->get());
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // RETURN VIEW
        return View::make($this->view.'add',[
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     * Creates a new object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object is created, filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'name'    => 'required|max:191',
            'room_number' => 'required|max:191|unique:Rooms,room_number,NULL,id,deleted_at,NULL'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // CREATE NEW ROOM
	    $room         = new Rooms;
	    // INSERT DATA
	    $room->name   = Input::get('name','');
	    $room->room_number = Input::get('room_number','');
	    // SAVE NEW ROOM
        $room->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'added', $room->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 3 buttons. "Submit" , that sends a POST request to postEdit() function to save the edited object,
     * "Cancel" , that redirects user to getIndex() view ,and "Delete", that sends a get request to getDelete()
     * function and deletes the object
     *
     * @param $id
     * @return mixed
     */
    public function getEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND ROOM
        $room = Rooms::findOrFail($id);
        // RETURN VIEW
        return View::make($this->view.'edit',[
            'item'  => $room,
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'name'    => 'required|max:191',
            'room_number' => 'required|max:191|unique:Rooms,room_number,'.$id.',id,deleted_at,NULL'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // FIND ITEM
        $room         = Rooms::findOrFail($id);
        // INSERT DATA
        $room->name   = Input::get('name','');
        $room->room_number = Input::get('room_number','');
        // SAVE ITEM
        $room->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $room->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     * Deletes an existing object and returns a redirect to getIndex() function
     *
     * Deletes an existing object if it is found. Logs succesful attempts. After successful delete returns a redirect
     * to getIndex() function. Can be called from getEdit() function view by a button "Delete"
     *
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND ROOM
        $room = Rooms::findOrFail($id);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'deleted', $room->id);
        // INSERT DATA
        $room->deleted_at = date('Y-m-d H:i:s');
        // SAVE DATA
        $room->save();
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }
}