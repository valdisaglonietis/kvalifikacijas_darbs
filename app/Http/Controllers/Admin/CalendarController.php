<?php namespace App\Http\Controllers\Admin;
use App\Models\Rooms;
use App\Models\Speciality;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\Models\UserActivities;
use View;
use Redirect;
use Validator;
use Session;
use Input;
use Lang;

use App\Models\Translations;
use App\Models\Investigations;
use App\Models\Patients;
use App\Models\Doctors;
use App\Models\ManipulationsGroups;
use App\Models\Diagnosis;
use App\Models\Services;
use App\Models\WorkingHours;
use App\User;
use DB;
use App\Models\Manipulations;

class CalendarController extends BaseController{
    protected $url = '/calendar/';
    protected $view = 'admin.calendar.';
    protected $title = 'calendar';

    /**
     * Returns view with fullcalendar
     *
     * Returns view which contains a fullcalendar. Fullcalendar send an ajax request to getList() function
     * and list of events are returned and displayed. Fullcalendar also sends an ajax request to 'rooms/list' to
     * get the list of all active rooms, that events are sorted by. When click on events user is redirected to
     * getEdit() function with specific ID or getAdd() if investigation_id is null(no investigation has been asigned).
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'title' => $this->title,
            'url' => $this->url
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by fullcalendar ajax request from getIndex() view.
     * Checks additional params from fullcalendar ajax request and queries the results
     * Start and End date inputs are required.
     *
     * @return string
     */
    public function getList() {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // CHECK REQUIRED PARAMS
        if(!Input::has('start')||!Input::has('end')){
            return json_encode([]);
        }
        $start = date('Y-m-d 00:00:00',strtotime(Input::get('start','')));
        $end = date('Y-m-d 23:59:59', strtotime(Input::get('end','')));
        // FIND WORKING HOURS IN INTERVAL
        $workingHours = WorkingHours::where('WorkingHours.start', '>=', $start)
            ->where('WorkingHours.end', '<=', $end);
        // JOIN ATTACHED FIELDS TO QUERY
        $workingHours = $workingHours->leftJoin('WorkingHoursGlobal', 'WorkingHours.working_hours_global_id', '=', 'WorkingHoursGlobal.id')
            ->leftJoin('Rooms', 'WorkingHoursGlobal.room_id', '=', 'Rooms.id')
            ->leftJoin('Users', 'WorkingHoursGlobal.user_id', '=', 'Users.id')
            ->leftJoin('Investigations', 'WorkingHours.investigation_id', '=', 'Investigations.id')
            ->leftJoin('Patients', 'WorkingHours.patient_id', '=', 'Patients.id');
        // SELECT AND EXECUTE QUERY
        $workingHours = $workingHours->select(
            'WorkingHours.id as id',
            'WorkingHours.start as start',
            'WorkingHours.end as end',
            'WorkingHours.status as status',
            'Rooms.id as resourceId',
            DB::raw("CONCAT(Rooms.name,'(',Rooms.room_number,')') as room"),
            DB::raw("CONCAT(Users.first_name,' ',Users.last_name) as title"),
            DB::raw("CONCAT(Patients.first_name,' ',Patients.last_name,'(',Patients.phone,')') as patient")
        )->get();
        // MODIFY DATA
        foreach ($workingHours as $item) {
            $item->start_date = date('d.m.Y', strtotime($item->start));
            $item->start_time = date('H:i', strtotime($item->start));
            $item->end_time =  date('H:i', strtotime($item->end));
            $item->url = $this->url.(($item->status)?"edit/".$item->id:"add/".$item->id);
            $item->className = ($item->status)?"edit_investigation":"add_investigation";
            $item->overlap = false;
            $item->description =
                Lang::get('admin.doctor').": <span>$item->title</span>
                </br>".
                Lang::get('admin.room').": <span>$item->room</span>
                </br>".
                Lang::get('admin.date_and_time').": <span>$item->start_date $item->start_time - $item->end_time</span>";
            if($item->status){
                $item->description .=
                    "</br>".
                    Lang::get('admin.patient').": <span>$item->patient</span>";
            }
        }
        // RETURN JSON ENCODED WORKING HOURS
        return json_encode($workingHours);
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd($id) {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        $workingHours = WorkingHours::findOrFail($id);
        $workingHours->start = formatDate($workingHours->start,'lv','l, d. F Y H:i') . ' - ' . formatDate($workingHours->end,'lv','H:i');
        // RETURN VIEW
        return View::make($this->view.'add',[
            'title' => $this->title,
            'url' => $this->url,
            'workingHours' => $workingHours
        ]);
    }

    /**
     * Creates a new object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object is created, filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'patient'    => 'required|exists:Patients,id',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // FIND PATIENT
        $patient = Patients::findOrFail(Input::get('patient'));
        // FIND WORKING HOURS
        $workingHours = WorkingHours::findOrFail($id);
        // CREATE NEW INVESTIGATION
        $investigation = new Investigations;
        // INSERT INVESTIGATON DATA
        $investigation->work_hours_id = $workingHours->id;
        // SAVE INVESTIGATION
        $investigation->save();
        // INSERT WORKING HOURS DATA
        $workingHours->patient_id = $patient->id;
        $workingHours->investigation_id = $investigation->id;
        $workingHours->status = 1;
        // SAVE WORKING HOURS
        $workingHours->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'created', $workingHours->id,$investigation->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 3 buttons. "Submit" , that sends a POST request to postEdit() function to save the edited object,
     * "Cancel" , that redirects user to getIndex() view ,and "Delete", that sends a get request to getDelete()
     * function and deletes the object
     *
     * @param $id
     * @return mixed
     */
    public function getEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND WORKING HOURS
        $workingHours = WorkingHours::findOrFail($id);
        // FIND INVESTIGATION
        $investigation = Investigations::findOrFail($workingHours->investigation_id);
        // RETURN VIEW
        return View::make($this->view.'edit',[
            'workingHours' => $workingHours,
            'investigation' => $investigation,
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @param $id
     * @return mixed
     */
    public function postEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'price'    => 'required|max:11',
            'notes' => 'nullable|max:65000'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // FIND WORKING HOURS
        $workingHours = WorkingHours::findOrFail($id);
        // FIND INVESTIGATION
        $investigation = Investigations::findOrFail($workingHours->investigation_id);
        // INSERT DATA
        $investigation->price = money_format('%.2n',(float)Input::get('price',''));
        $investigation->notes = Input::get('notes','');
        // SAVE INVESTIGATION
        $investigation->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $workingHours->id, $investigation->id);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url.'edit/'.$workingHours->id);
    }

    /**
     * Deletes an existing object and returns a redirect to getIndex() function
     *
     * Deletes an existing object if it is found. Logs succesful attempts. After successful delete returns a redirect
     * to getIndex() function. Can be called from getEdit() function view by a button "Delete"
     *
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'delete');
        // FIND WORKING HOURS
        $workingHours = WorkingHours::findOrFail($id);
        // FIND INVESTIGATION
        $investigation = Investigations::findOrFail($workingHours->investigation_id);
        // INSERT WORKING HOURS DATA
        // CREATE REPLACEMENT FOR WORKING HOURS
        $workingHoursNew = new WorkingHours;
        $workingHoursNew->working_hours_global_id = $workingHours->working_hours_global_id;
        $workingHoursNew->start = $workingHours->start;
        $workingHoursNew->end = $workingHours->end;
        $workingHoursNew->status = 0;
        $workingHoursNew->investigation_id = null;
        $workingHoursNew->patient_id = null;
        $workingHoursNew->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'deleted',$workingHours->id,$investigation->id);
        // DELETE INVESTIGATION
        $investigation->delete();
        // DELETE WORKING HOURS
        $workingHours->delete();
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }
}
