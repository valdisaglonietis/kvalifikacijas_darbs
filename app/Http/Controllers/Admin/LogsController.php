<?php 
namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use View;
use App\Models\UserActivities;
use Input;
use DB;


class LogsController extends BaseController{
    private $title  = 'logs';
    private $url = '/logs/';
    private $view = 'admin.logs.';

    /**
     * Returns view with datatable
     *
     * Returns view with datatable. Datatable executes an ajax get request to getList() function
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'url' => $this->url,
            'title' => $this->title
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by datatable ajax request from getIndex() view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @return string
     */
    public function getList(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // FIND ALL USER ACTIVITIES
        $userActivities = UserActivities::whereNull('deleted_at')
            ->leftJoin('Users','UserActivities.userid','=','Users.id');
        // CHECK QUERY PARAMETERS
        if(Input::has('datatable')) {
            $datatable = Input::get('datatable');
        }

        if(isset($datatable) && isset($datatable['sort'])) {
            $userActivities = $userActivities->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }

        if(Input::has('query') && strlen(Input::get('query')['generalSearch']) > 0) {
            $searchQuery = Input::get('query')['generalSearch'];
        }

        // SEARCH QUERY
        if(isset($searchQuery)) {
            $userActivities = $userActivities
                ->Where('UserActivities.id', 'LIKE', "%$searchQuery%")
                ->orWhere('UserActivities.action','LIKE',"%$searchQuery%")
                ->orWhere('UserActivities.module','LIKE',"%$searchQuery%")
                ->orWhere('UserActivities.extra_id','LIKE',"%$searchQuery%")
                ->orWhere('UserActivities.extra_text','LIKE', "%$searchQuery")
                ->orWhere(DB::raw("CONCAT(UserActivities.created_at,' ')"),'LIKE',"%$searchQuery%")
                ->orWhere(DB::raw("CONCAT(Users.first_name,' ',Users.last_name)"),'LIKE',"%$searchQuery%");
        }
        $userActivities = $userActivities->select(
            DB::raw("CONCAT(Users.first_name,' ',Users.last_name) as user"),
            'UserActivities.action as action',
            'UserActivities.module as module',
            'UserActivities.extra_id as object',
            'UserActivities.extra_text as additional_info',
            'UserActivities.created_at as time'
        );
        // EXECUTE QUERY AND RETURN ENCODED USER ACTIVITIES
        return json_encode($userActivities->get());
    }
}