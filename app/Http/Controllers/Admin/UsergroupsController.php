<?php namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use App\Models\UsersFilialesJoins;
use Auth;
use App\Models\Patients;
use Str;
use App\Models\RoleHasPermissions;
use Validator;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

class UsergroupsController extends BaseController{
    protected $url = '/user-groups/';
    protected $view = 'admin.user-groups.';
    protected $title = 'user-groups';

    /**
     * Returns view with datatable
     *
     * Returns view which contains a datatable. Datatable send an ajax request to getList() function
     * and list of objects are returned and displayed
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
        return View::make($this->view.'index',[
            'url'         => $this->url,
            'title'       => $this->title,
        ]);
    }

    /**
     * Returns json encoded list of objects
     *
     * Returns json encoded list of objects.
     * Is called by datatable ajax request from getIndex() view.
     * Checks additional params from datatable ajax request and queries the results
     *
     * @return string
     */
    public function getList() {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // GET ALL USER GROUPS
        $userGroups = Role::select('roles.id','roles.key');
        // CHECK QUERY
        if(Input::has('datatable')){
            $datatable = Input::get('datatable');
        }
        if(isset($datatable) && isset($datatable['sort'])){
            $userGroups = $userGroups->orderBy($datatable['sort']['field'],$datatable['sort']['sort']);
        }else{
            $userGroups = $userGroups->orderBy('roles.id','desc');
        }
        if(Input::has('query') && strlen(Input::get('query')['generalSearch']) > 0){
            $searchQuery = Input::get('query')['generalSearch'];
        }
        // SEARCH QUERY
        if(isset($searchQuery)){
            $userGroups = $userGroups
                ->where('roles.key','LIKE',"%$searchQuery%")
                ->orWhere('roles.id','LIKE',"%$searchQuery%");

        }
        // EXECUTE QUERY AND RETURN ENCODED USER GROUPS
        return json_encode($userGroups->get());
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // RETURN VIEW
        return View::make($this->view.'add',[
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     * Creates a new object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * a new object is created, filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // VALIDATE
        $rules = [
            'title'    => 'required|unique:roles,name,slug|max:255',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        // CREATE USER GROUP
        $userGroup = Role::create(['name' => Str::slug(Input::get('title'))]);
        // INSERT DATA
        $userGroup->key = Input::get('title');
        // SAVE USER GROUP
        $userGroup->save();
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'created', $userGroup->id,$userGroup->key);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.created'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     *
     * Returns a view with a form to edit an existing object
     *
     * Returns a view with a form to edit an existing object. The view displays one or more already filled fields
     * and has 3 buttons. "Submit" , that sends a POST request to postEdit() function to save the edited object,
     * "Cancel" , that redirects user to getIndex() view ,and "Delete", that sends a get request to getDelete()
     * function and deletes the object
     *
     * @param $id
     * @return mixed
     */
    public function getEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND USER GROUP
        $userGroup = Role::findOrFail($id);
        // GET ALL PERMISSION TYPES
        $permission_types = Permission::distinct('action')->pluck('action')->toArray();
        sort($permission_types);
        // FIND AND SET ALL PERMISSIONS
        $all = array_search('all',$permission_types);
        if(isset($all) && $all){
            $permission_types[$all] = $permission_types[0];
            $permission_types[0] = 'all';
        }
        // SORT ALL MODULE PERMISSIONS FOR OUTPUT
        $permission_modules = [];
        foreach(Permission::distinct('module')->pluck('module') as $key => $permission_module){
            $permission_modules[$key]['module'] = $permission_module;
            $allowed_permissions =Permission::where('module',$permission_module)->get();
            foreach($allowed_permissions as $allowed_permission){
                $permission_modules[$key][$allowed_permission->action] = $allowed_permission->id;
            }

        }
        // RETURN VIEW
        return View::make($this->view.'edit',[
            'permissions' =>$userGroup->permissions->pluck('id')->toArray(),
            'userGroup' => $userGroup,
            'title' => $this->title,
            'url' => $this->url,
            'permission_types' => $permission_types,
            'permission_modules' => $permission_modules,
        ]);
    }

    /**
     *
     * Edits an existing object and returns to getIndex() view or returns back with input and errors
     *
     * Validates the input. If input is not valid then returns back with input and errors. If input is valid, then
     * an existing object is filled with data and saved. Logs successful attempts. At the end returns a view that
     * is a redirect to getIndex() function
     *
     * @param null $id
     * @return mixed
     */
    public function postEdit($id){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // FIND USER GROUP
        $userGroup                   = Role::findOrFail($id);
        // INSERT DATA
        $userGroup->key            = Input::get('key','');
        // SAVE USER GROUP
        $userGroup->save();
        // SAVES NEW PERMISSIONS
        if(Input::has('permissions')) {
            // DELETES OLD PERMISSIONS
            $q = 'DELETE FROM role_has_permissions where role_id = '.$userGroup->id;
            \DB::delete($q);
            // CREATES NEW PERMISSIONS
            foreach (Input::get('permissions') as $key => $permission) {
                $new_permission = new RoleHasPermissions;
                $new_permission->role_id = $userGroup->id;
                $new_permission->permission_id = $key;
                $new_permission->save();
            }
        }
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited', $userGroup->id,$userGroup->key);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     * Deletes an existing object and returns a redirect to getIndex() function
     *
     * Deletes an existing object if it is found. Logs succesful attempts. After successful delete returns a redirect
     * to getIndex() function. Can be called from getEdit() function view by a button "Delete"
     *
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        // CHECK PERMISSIONS
        if(!Auth::user()->super_admin) {
            Session::put('notify', ['message' => Lang::get('admin.no-permissions'), 'type' => 'warning']);
            return Redirect::to($this->url);
        }
        // FIND USER GROUP
        $userGroup = Role::findOrFail($id);
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'deleted', $userGroup->id,$userGroup->key);
        // DELETE USER GROUP
        $userGroup->delete();
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.deleted'), 'type' => 'danger']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }
}