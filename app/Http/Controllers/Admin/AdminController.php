<?php
namespace App\Http\Controllers\Admin;
use Erisinajumi\Crud\Models\CrudModule;
use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController{
    protected $title = 'dashboard';
    protected $url = '/dashboard/';
    protected $view = 'admin.dashboard.';

    /**
     * Returns view with trending and important news
     *
     * Returns view with trending and important news
     *
     * @return mixed
     */
	public function getIndex(){
	    // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // RETURN VIEW
		return \View::make($this->view.'index',[
            'title' => $this->title,
        ]);
	}


}
