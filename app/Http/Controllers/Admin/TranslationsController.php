<?php namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use App\Models\UsersFilialesJoins;
use Auth;
use Config;
use Cookie;
use Str;

class TranslationsController extends BaseController{
    protected $view = 'admin.translations.';
    protected $title = 'translations';
    protected $url = "/translations/";
    protected $folder = 'admin.php';

    /**
     * Returns view with datatable
     *
     * Returns view which contains a datatable. Datatable send an ajax request to getList() function
     * and list of objects are returned and displayed
     *
     * @return mixed
     */
    public function getIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');

        // RETURN VIEW
        return View::make($this->view.'index',[
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     * Saves edited translations and returns a redirect to getIndex() function
     *
     * Searches through every translations folder and target file and replaces edited translations
     * Returns a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postIndex(){
        // CHECK PERMISSIONS
        hasPermissions($this->title,'edit');
        // REDIRECT IF NO TRANSLATIONS
        if(!Input::has('translations')) return Redirect::to($this->url);
        // SET VARIABLES
        $translations = Input::get('translations');
        $path = app_path().'/../resources/lang/';
        // OVERWRITE EDITED TRANSLATIONS FOR EACH LANGUAGE
        foreach($translations as $lang => $translation){
            // SET VARIABLES
            $fileName = app_path().'/../resources/lang/'.$lang.'/'.$this->folder;
            $replaced = false;
            // OPEN FILES
            $reading = fopen($fileName, 'r');
            $writing = fopen($fileName.'.tmp', 'w');
            // READ EVERY LINE OF THE FILE
            while (!feof($reading)) {
                $line = fgets($reading);
                // SEARCH EVERY TRANSLATION IN THE READ LINE
                foreach($translation as $key => $transl){
                    // REPLACE WITH NEW LINE WITH NEW TRANSLATION
                    if (stristr($line,"'".$key."' =>")) {
                        $line = "'$key' => '".self::sanitizeInput($transl)."',\n";
                        $replaced = true;
                    }
                }
                // WRITE LINE IN TEMP FILE
                fputs($writing, $line);
            }
            // CLOSE FILES
            fclose($reading); fclose($writing);
            // RENAME ONLY IF ANYTHING REPLACED
            if ($replaced)
            {
                rename($fileName.'.tmp', $fileName);
            } else {
                unlink($fileName.'.tmp');
            }
        }
        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'edited',null,null);
        // OUTPUT MESSAGE
        Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    /**
     * Returns a view with a form to add a new object
     *
     * Returns a view with a form to add a new object. The view displays one or more fields to fill
     * and has 2 buttons. "Submit" , that sends a POST request to postAdd() function to create a new object and "Cancel"
     * that redirects user to getIndex() view
     *
     * @return mixed
     */
    public function getAdd() {
        // CHECK PERMISSIONS
        if(!Auth::user()->super_admin) return back();
        // RETURN VIEW
        return View::make($this->view.'add',[
            'title' => $this->title,
            'url' => $this->url,
        ]);

    }

    // POST NEW TRANSLATION

    /**
     * saved a new translation and returns a redirect to getIndex() function
     *
     * goes through every language folder and inserts a new translation at the end of every language file
     * Aborts if translations already exists. Returns a redirect to getIndex() function
     *
     * @return mixed
     */
    public function postAdd() {
        // CHECK PERMISSIONS
        if(!(Auth::user()->super_admin && Input::has('key'))) return back();
        // SET VARIABLES
        $path = app_path().'/../resources/lang/';
        $translation_key = Str::slug(Input::get('key'));
        $aborted = false;
        // ADD NEW TRANSLATION FOR EACH LANGUAGE
        foreach(config('application.languages') as $lang){
            // SET VARIABLES
            $fileName = $path.$lang.'/'.$this->folder;
            $replaced = false;
            // OPEN FILES
            $reading = fopen($fileName, 'r');
            $writing = fopen($fileName.'.tmp', 'w');
            // READ EVERY LINE OF THE FILE
            while (!feof($reading)) {
                $line = fgets($reading);
                // IF TRANSLATION IS FOUND ,ABORT THE INSERT
                if (stristr($line,"'$translation_key' => '")) {
                    $aborted = true;
                }
                // IF LAST LINE IS FOUND , INSERT THE TRANSLATION
                if( $line === '];' && !$aborted){
                    $newTranslation = "'$translation_key' => '".self::sanitizeInput(Input::get($lang))."',\n";
                    fputs($writing, $newTranslation);
                    $replaced = true;
                }
                // WRITE LINE IN TEMP FILE
                fputs($writing, $line);
            }
            // CLOSE FILES
            fclose($reading); fclose($writing);
            // might as well not overwrite the file if we didn't replace anything
            if ($replaced && !$aborted)
            {
                rename($fileName.'.tmp', $fileName);
            } else {
                unlink($fileName.'.tmp');
            }
        }

        // LOG ACTIVITIES
        UserActivities::registerV2($this->title,'added',null,$translation_key);
        // OUTPUT MESSAGE
        if(!$aborted){
            Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
        }else{
            Session::put('notify', ['message' => Lang::get('admin.translation-exists'), 'type' => 'warning']);
        }
        // REDIRECT TO LIST
        return Redirect::to($this->url);
    }

    // GET ALL TRANSLATIONS

    /**
     * Returns json encoded array of translation arrays

     * Returns json encoded array of translation arrays. First the folders are read and saved in array for
     * each language. Then main language is selected and for every translation a translation array is created
     * and saved into the array of translation arrays. For every translation array there is key, count and
     * translation for each language.
     *
     *
     * @return string
     */
    public function getList() {
        // CHECK PERMISSIONS
        hasPermissions($this->title,'view');
        // SET VARIABLES
        $path = app_path().'/../resources/lang/';
        $folders = scandir($path);
        $contents = [];
        // READ ALL TRANSLATIONS BY FOLDERS
        foreach($folders as $folder){
            if($folder != '..' || $folder != '.'){
                // LOAD FOLDER IF IT EXISTS
                if(file_exists($path.$folder.'/'.$this->folder)){
                    $contents[$folder] = include($path.$folder.'/'.$this->folder);
                }
            }
        }
        // SET VARIABLES
        $translations = [];
        $count = 0;
        // SORT TRANSLATIONS INTO CONVENIENT ARRAY GOING THROUGH MAIN LANGUAGE
        foreach($contents[config('application.language')] as $key => $content){
            // CREATE TRANSLATION ARRAY
            $translation = [];
            // GO THROUGH EVERY LANGUAGE AND GET TRANSLATION FOR EVERY LANGUAGE
            foreach(config('application.languages') as $language){
                $translation[$language] = isset($contents[$language]) && isset($contents[$language][$key])?$contents[$language][$key]:'';
            }
            $translation['id'] = $count;
            $translation['key'] = $key;
            // INSERT TRANSLATIONS ARRAY INTO TRANSLATIONS ARRAY
            $translations[] = $translation;
            $count++;
        }
        // RETURN ENCODED TRANSLATIONS
        return json_encode($translations);
    }

    /**
     * returns sanitized string
     *
     * returns sanitized string
     *
     * @param $input
     * @return string
     */
    private function sanitizeInput($input){
        // RETURN SANITIZED INPUT
        return str_replace("'","\'",htmlspecialchars($input));
    }
}