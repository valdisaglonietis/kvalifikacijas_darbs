<?php namespace App\Http\Controllers\Admin;
use Illuminate\Routing\Controller as BaseController;
use App\Access;
use App\User;
use App\Models\UserActivities;
use View;
use App\Models\Department;
use Input;
use Hash;
use Redirect;
use Session;
use Lang;
use App\Models\UsersFilialesJoins;
use Auth;
use Config;
use Cookie;
use Mail;

class PasswordRecoveryController extends BaseController{
    protected $view = 'admin.password_recovery.';
    protected $title = 'password_recovery';
    protected $url = "/password_recovery/";

    /**
     * Returns view with Recovery form
     *
     * Returns view with Recovery form. Can be only used 1 time
     *
     * @param $token
     * @return mixed
     */
    public function getIndex($token){
        // FIND USER WITH RECOVERY TOKEN
        $user = User::where('recovery_token',$token)->where('recovery_due_date','>=',date("Y-m-d H:i:s",time()))->where('recovery_used',0)->first();
        // SET USED SO CANT USE LINK AGAIN
        if($user){
            $user->recovery_used = 1;
            $user->save();
        }
        // RETURN VIEW
        return View::make($this->view.'index',[
            'user' => $user,
            'title' => $this->title,
            'url' => $this->url,
        ]);
    }

    /**
     * Returns response string about password change
     *
     * Returns response string. String success:password_changed is returned if password pas changed. Otherwise
     * error string is returned.
     *
     * @param $token
     * @return string
     */
    public function postIndex($token){
        // FIND USER WITH RECOVERY TOKEN
        $user = User::where('recovery_token',$token)->where('recovery_due_date','>=',date("Y-m-d H:i:s",time()))->where('recovery_used',1)->first();
        // VALIDATE PASSWORD RECOVERY
        if(!(isset($user) && $user)){
            return 'error:token_expired';
        }
        // VALIDATE PASSWORD
        if(Input::has('password') && Input::has('confirm_password') && Input::get('password') == Input::get('confirm_password')){
            $password = Input::get('password');
            if(strlen($password) < 8){
                return 'error:password_is_too_short_standard_8';
            }
            $user->password = bcrypt($password);
            $user->recovery_due_date = '1970-01-01 00:00:00';
            $user->attempts = 0;
            $user->recovery_used = 3;
            $user->save();
            // OUTPUT MESSAGE
            Session::put('notify', ['message' => Lang::get('admin.saved'), 'type' => 'success']);
            return 'success:password_changed';
        }else{
            return 'error:passwords_dont_match';
        }
    }

    /**
     * Returns a response string with information about status of email recovery
     *
     * Returns a response string with information about status of email recovery. If email was found a string with
     * success information is returned. Otherwise string with error information is returned.
     *
     * @return string
     * @throws \Exception
     */
    public function postRequest(){
        //  CHECK EMAIL
        if(!Input::has('email')) return 'error:email_not_found';
        // FIND USER
        $user = User::where('email',Input::get('email'))->first();
        // RETURN IF NO USER WAS FOUND
        if(!(isset($user) && $user)) return 'error:email_not_found';
        // INSERT DATA
        $user->recovery_token = bin2hex(random_bytes(99));
        $user->recovery_due_date = date("Y-m-d H:i:s",strtotime("+1 day"));
        $user->recovery_used = 0;
        // SAVE USER
        $user->save();
        // GET VARIABLES
        $from = Config::get('mail.from');
        // SEND EMAIL
        Mail::send('admin.password_recovery.email_recovery', ['user' => $user], function ($m) use ($user,$from) {
            $m->from($from['address'],$from['name']);
            $m->to($user->email,$user->full_name)->subject(Lang::get('admin.password_recovery'));
        });
        // RETURN RESPONSE
        return 'success:email_found';
    }
}
