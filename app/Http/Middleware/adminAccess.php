<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;
use Auth;

class adminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(\Auth::guest()){
            return \Redirect::to('/login');
	    } else {
			return $next($request);
	    }
    }
}
