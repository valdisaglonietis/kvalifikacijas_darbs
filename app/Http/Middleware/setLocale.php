<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Config;
use Illuminate\Support\Facades\Cookie;
use Auth;

class setLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if($request->has('language') && in_array($request->get('language'),Config::get('application.languages'))) {
//            Cookie::queue('locale', $request->get('language'), 2147483647, '/');
//            return redirect()->to($request->url());
//        }

        $locale = '';

        if(Cookie::has('locale') && in_array(Cookie::get('locale'),Config::get('application.languages'))){
            $locale = Cookie::get('locale');
        }elseif(!Auth::guest() && in_array(Auth::user()->language,Config::get('application.languages'))){
            $locale = Auth::user()->language;
            Cookie::queue('locale', $locale, 2147483647,'/');
        }else{
            // TODO should combine browserlocale and default language for site
            $browserLocale = '';//substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            $locale = in_array($browserLocale,Config::get('application.languages'))?$browserLocale:Config::get('application.language');
            Cookie::queue('locale', $locale, 2147483647,'/');
        }
        App::setLocale($locale);
        return $next($request);
    }
}
