<?php	
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Lang;
use Illuminate\Support\Facades\App;
use Config;
use Auth;

class UserActivities extends Model {
    use HasRoles;

    protected $guard_name = 'web';
    protected $table = 'UserActivities';
    protected $guarded = ['id'];
    
    public function user(){
        return $this->belongsTo('App\User', 'userid');
    }

    public static function register($page = '', $name = '', $extraid1 = 0, $extraid2 = '') {
        $user =  \Auth::user();
        $logs = UserActivities::create(
            array(
                'action' => $name,
                'module' => $page,
                'userid' => $user->id,
                'extra_id' => $extraid1,
                'extra_text' => $extraid2
            )
        );
    }
    public static function registerV2($module,$action,$extra_id = null,$extra_text = null) {
        $savedLocale = App::getLocale();
        App::setLocale(Config::get('application.language'));
        UserActivities::create([
            'action' => Lang::get('admin.'.$action),
            'module' => Lang::get('admin.'.$module),
            'userid' => Auth::user()->id,
            'extra_id' => $extra_id,
            'extra_text' => $extra_text
        ]);
        App::setLocale($savedLocale);
    }


}