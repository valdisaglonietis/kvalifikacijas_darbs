<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PatientActivity extends Model {
    protected $guard_name = 'web';
    protected $table = 'PatientActivity';

    public static function log($activity, $patient_id) {
        $log = new PatientActivity();
        $log->patient_id = $patient_id;
        $log->activity = $activity;
        $log->user_id = \Auth::user()->id;
        $log->save();

        return $log;
    }
}