<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Lang;

class Patients extends Model {

    use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';
    protected $table = 'Patients';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    
    
    public function formatedData(){
	    return $this->first_name.' '.$this->last_name." (ID: $this->id)";
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}