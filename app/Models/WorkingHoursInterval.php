<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class WorkingHoursInterval extends Model {


    protected $guard_name = 'web';
    protected $table = 'WorkingHoursInterval';
    protected $guarded = ['id'];


    public function get_workinghoursglobal(){
        return $this->belongsTo('App\Models\WorkingHoursGlobal', 'working_hours_global_id');
    }
}
