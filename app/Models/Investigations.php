<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Lang;

class Investigations extends Model {

    use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';
    protected $table = 'Investigations';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
}