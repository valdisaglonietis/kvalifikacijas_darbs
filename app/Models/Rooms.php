<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Rooms extends Model {

    use HasRoles, SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'Rooms';
}