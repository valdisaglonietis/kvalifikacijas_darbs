<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class WorkingHours extends Model {

	use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';
	protected $table = 'WorkingHours';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    
    
    public function get_room(){
		return $this->belongsTo('App\Models\Rooms', 'room_id');
    }

    public function get_Patient(){
		return $this->belongsTo('App\Models\Patients', 'patient_id');
    }
   
    public function get_GlobalHours() {
        return $this->belongsTo('App\Models\WorkingHoursGlobal', 'working_hours_global_id');
    }
}
