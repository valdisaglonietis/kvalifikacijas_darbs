<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class WorkingHoursGlobal extends Model {

	use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';
	protected $table = 'WorkingHoursGlobal';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];

	public function user(){
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}
}
