<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class RoleHasPermissions extends Model {

    protected $guard_name = 'web';
    protected $table = 'role_has_permissions';
}