<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// CACHE CLEAR
if (getenv('APP_ENV') == 'local') Artisan::call('view:clear', []);

// GLOBAL CUSTOM FUNCTIONS
require_once app_path().'/libraries/custom/tools.php';

Route::group(array('prefix' => '/'), function()
{
    // GUEST MIDDLEWARE
    Route::group(['middleware' => 'guest'], function(){
        // LOGIN ROUTES
        Route::get('login', 'Admin\LoginController@getLogin');
        Route::post('login', 'Admin\LoginController@postLogin');
        // PASSWORD RECOVERY ROUTES
        Route::post('password_recovery/request', 'Admin\PasswordRecoveryController@postRequest');
        Route::get('password_recovery/{token}', 'Admin\PasswordRecoveryController@getIndex');
        Route::post('password_recovery/{token}', 'Admin\PasswordRecoveryController@postIndex');
    });

    // USER MIDDLEWARE
    Route::group(['middleware' => 'user'], function() { // user - access and locale together
        // DASHBOARD
        Route::get('/', 'Admin\AdminController@getIndex');
        // LOGOUT
        Route::get('login/logout', 'Admin\LoginController@getLogout');
        // USER
        Route::get('users', 'Admin\UsersController@getIndex');
        Route::get('users/list', 'Admin\UsersController@getList');
        Route::get('users/add', 'Admin\UsersController@getAdd');
        Route::post('users/add', 'Admin\UsersController@postAdd');
        Route::get('users/edit/{id}', 'Admin\UsersController@getEdit');
        Route::post('users/edit/{id}', 'Admin\UsersController@postEdit');
        Route::get('users/delete/{id}', 'Admin\UsersController@getDelete');
        Route::get('users/logs/{id}', 'Admin\UsersController@getLogs');
        // USER PROFILE
        Route::get('users/profile', 'Admin\UsersController@getProfile');
        Route::post('users/profile', 'Admin\UsersController@postProfile');
        // USERGROUPS
        Route::get('user-groups', 'Admin\UsergroupsController@getIndex');
        Route::get('user-groups/list', 'Admin\UsergroupsController@getList');
        Route::get('user-groups/add', 'Admin\UsergroupsController@getAdd');
        Route::post('user-groups/add', 'Admin\UsergroupsController@postAdd');
        Route::get('user-groups/edit/{id}', 'Admin\UsergroupsController@getEdit');
        Route::post('user-groups/edit/{id}', 'Admin\UsergroupsController@postEdit');
        Route::get('user-groups/delete/{id}', 'Admin\UsergroupsController@getDelete');
        // TRANSLATIONS
        Route::get('translations', 'Admin\TranslationsController@getIndex');
        Route::post('translations', 'Admin\TranslationsController@postIndex');
        Route::get('translations/list', 'Admin\TranslationsController@getList');
        Route::get('translations/add', 'Admin\TranslationsController@getAdd');
        Route::post('translations/add', 'Admin\TranslationsController@postAdd');
        // LOGS
        Route::get('logs', 'Admin\LogsController@getIndex');
        Route::get('logs/list', 'Admin\LogsController@getList');
        // ROOMS
        Route::get('rooms', 'Admin\RoomController@getIndex');
        Route::get('rooms/list', 'Admin\RoomController@getList');
        Route::get('rooms/add', 'Admin\RoomController@getAdd');
        Route::post('rooms/add', 'Admin\RoomController@postAdd');
        Route::get('rooms/edit/{id}', 'Admin\RoomController@getEdit');
        Route::post('rooms/edit/{id}', 'Admin\RoomController@postEdit');
        Route::get('rooms/delete/{id}', 'Admin\RoomController@getDelete');
        // WORKING HOURS
        Route::get('working-hours', 'Admin\WorkingHoursController@getIndex');
        Route::get('working-hours/list', 'Admin\WorkingHoursController@getList');
        Route::get('working-hours/edit/{id?}', 'Admin\WorkingHoursController@getEdit');
        Route::post('working-hours/edit/{id?}', 'Admin\WorkingHoursController@postEdit');
        Route::get('working-hours/delete/{id}', 'Admin\WorkingHoursController@getDelete');
        Route::get('working-hours/delete-all/{id}', 'Admin\WorkingHoursController@getDeleteAll');
        // CALENDAR
        Route::get('calendar', 'Admin\CalendarController@getIndex');
        Route::get('calendar/list', 'Admin\CalendarController@getList');
        Route::get('calendar/add/{id}', 'Admin\CalendarController@getAdd');
        Route::post('calendar/add/{id}', 'Admin\CalendarController@postAdd');
        Route::get('calendar/edit/{id}', 'Admin\CalendarController@getEdit');
        Route::post('calendar/edit/{id}', 'Admin\CalendarController@postEdit');
        Route::get('calendar/delete/{id}', 'Admin\CalendarController@getDelete');
        // PATIENTS
        Route::get('patients', 'Admin\PatientsController@getIndex');
        Route::get('patients/list', 'Admin\PatientsController@getList');
        Route::get('patients/add', 'Admin\PatientsController@getAdd');
        Route::post('patients/add', 'Admin\PatientsController@postAdd');
        Route::get('patients/edit/{id}', 'Admin\PatientsController@getEdit');
        Route::post('patients/edit/{id}', 'Admin\PatientsController@postEdit');
        Route::get('patients/delete/{id}', 'Admin\PatientsController@getDelete');
        Route::get('patients/health-history/{id}', 'Admin\PatientsController@getHealthHistory');
    });
});
