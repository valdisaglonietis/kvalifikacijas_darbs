<?php
	return [
		'month_1' 	=> 'Janvāris',
		'month_2' 	=> 'Februāris',
		'month_3' 	=> 'Marts',
		'month_4' 	=> 'Aprīlis',
		'month_5' 	=> 'Maijs',
		'month_6' 	=> 'Jūnijs',
		'month_7' 	=> 'Jūlijs',
		'month_8' 	=> 'Augusts',
		'month_9' 	=> 'Septembris',
		'month_10' 	=> 'Oktobris',
		'month_11' 	=> 'Novembris',
		'month_12' 	=> 'Decembris',
		'events__calendar_title' => 'Tiesas sēžu kalendārs',
		
		'events__calendar_weekday_1' => 'PR',
		'events__calendar_weekday_2' => 'OT',
		'events__calendar_weekday_3' => 'TR',
		'events__calendar_weekday_4' => 'CE',
		'events__calendar_weekday_5' => 'PK',
		'events__calendar_weekday_6' => 'SE',
		'events__calendar_weekday_7' => 'SV',
		
		'events__codes_PAC' 	=> 'Civillietu tiesu palāta',
		'events__codes_PAC_contacts' 	=> 'Kancelejas tālrunis 67020347',
		'events__codes_PAK' 	=> 'Krimināllietu tiesu palāta',
		'events__codes_PAK_contacts' => 'Kancelejas tālrunis 67020330',
		'events__codes_SKA' => 'Administratīvo lietu departaments',
		'events__codes_SKA_contacts' => 'Kancelejas tālrunis 67020339',
		'events__codes_SKC' => 'Civillietu departaments',
		'events__codes_SKC_contacts' => 'Kancelejas tālrunis 67020364',
		'events__codes_SKK' => 'Krimināllietu departaments',
		'events__codes_SKK_contacts' => 'Kancelejas tālrunis 67020361',
		
		'events__fields_accused' => 'Apsūdzētais', 
		'events__fields_articles' => 'Panti', 
		'events__fields_essence' => 'Būtība', 
		'events__fields_claimant' => 'Prasītājs', 
		'events__fields_defendant' => 'Atbildētājs', 
		
		
		'clearfilter' => 'Noņemt Filtru',
		
		'judicialbody' => 'Iepriekšejā instance',
		
		'dateofruling' => 'Tiesas sēdes datums',
		'from' => 'no',
		'too' => 'līdz',
		
		'caseno' => 'Lietas Nr.',
		'proceeded' => 'Apsūdzētais (Dalībnieki)',
		'or' => 'vai',
		'serach' => 'atlasīt',
		
		'hearing' => 'Tiesas sēde',
		'hall' => 'Zāle',
		
		'rezault' => 'Rezultāts',
		'instance' => 'Instance',
		
	   
		
	];
?>