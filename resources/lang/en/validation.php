<?php return [
    'required' => ':attribute lauks ir obligāts',
    'min'=>':attribute jābūt vismaz :min simboliem',
    'confirmed'=>':attribute nesakrīt',
    'email'=>'Nederīga e-pasta adrese',
    'unique'=>'Lietotājs, ar šādu e-pasta adresi, jau eksistē.'
];