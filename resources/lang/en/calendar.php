<?php return array (
  'month_1' => 'January',
  'month_2' => 'February',
  'month_3' => 'March',
  'month_4' => 'April',
  'month_5' => 'May',
  'month_6' => 'June',
  'month_7' => 'July',
  'month_8' => 'August',
  'month_9' => 'September',
  'month_10' => 'October',
  'month_11' => 'November',
  'month_12' => 'December',
  'events__calendar_title' => 'Hearings calendar',
  'events__calendar_weekday_1' => 'PR',
  'events__calendar_weekday_2' => 'OT',
  'events__calendar_weekday_3' => 'TR',
  'events__calendar_weekday_4' => 'CE',
  'events__calendar_weekday_5' => 'PK',
  'events__calendar_weekday_6' => 'SE',
  'events__calendar_weekday_7' => 'SV',
  'events__codes_PAC' => 'The chamber of civil cases',
  'events__codes_PAC_contacts' => 'Office phone 67020347',
  'events__codes_PAK' => 'The chamber of criminal cases',
  'events__codes_PAK_contacts' => 'Office phone 67020330',
  'events__codes_SKA' => 'The department of administrative cases',
  'events__codes_SKA_contacts' => 'Office phone 67020339',
  'events__codes_SKC' => 'Civil department',
  'events__codes_SKC_contacts' => 'Office phone 67020364',
  'events__codes_SKK' => 'The criminal department',
  'events__codes_SKK_contacts' => 'Office phone 67020361',
  'events__fields_accused' => 'The accused',
  'events__fields_articles' => 'Articles',
  'events__fields_essence' => 'The essence of',
  'events__fields_claimant' => 'The plaintiff',
  'events__fields_defendant' => 'The defendant',
  
  
	'judicialbody' => 'Judicial body',
	'clearfilter' => 'Remove filter',
	
	'dateofruling' => 'Hearing date',
	'from' => 'from',
	
	
	'caseno' => 'Case No.',
	'proceeded' => 'The accused (Participants)',
	'or' => 'or',
	'serach' => 'select',
	
	'too' => 'to',
	
	'hearing' => 'Hearing',
		'hall' => 'Room',
		
		'rezault' => 'Result',
 
		'instance' => 'Instance',
  
  
);
