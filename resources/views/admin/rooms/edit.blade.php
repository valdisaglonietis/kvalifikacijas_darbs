@extends('admin.layout.main')
@section('content')
    <form method="post" action="" class="form-horizontal">
        {{ csrf_field() }}
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
                        <i class="flaticon-app"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        @lang('admin.'.$title): @lang('admin.editing')
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="actions btn-set">
                    <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save')</button>
                    <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                    <a onclick="return confirm('@lang('admin.confirm')');"  href="{{ url($url.'delete/'.$item->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger">@lang('admin.delete')</a>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-body form">
                <div class="form-row-seperated">
                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.title'):<span class="required">*</span></label>
                        <div class="col-md-10">
                            <input required type="text" class="form-control" name="name" value="{{Input::old('name',$item->name)}}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.room_number'):<span class="required">*</span></label>
                        <div class="col-md-10">
                            <input required type="text" class="form-control" name="room_number" value="{{Input::old('room_number',$item->room_number)}}" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
@section('scripts')
@endsection
@section('css')
@endsection
