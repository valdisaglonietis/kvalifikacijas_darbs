<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			@lang('admin.main_title')
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Base Styles -->
		<link href="/admin_theme/additional/webfont/default_local.min.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="/admin_theme/assets/demo/default/media/img/logo/glc-logo-rgb_burned.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-color:#9aaed4;">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
                                <img style="color:#9aaed4; width:150px;" src="/media/default/logo_bright.svg">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									@lang('admin.login_to_your_acc')
								</h3>
								<div id="alert_email_sent" style="display:none" class="alert alert-success alert-dismissible fade show" role="alert">
									<button onclick="alert_email_sent_close()" type="button" class="close" aria-label="Close"></button>
									<strong>
										@lang('admin.email_found_and_sent')
									</strong>
								</div>
							</div>
							<form class="m-login__form m-form" action="" method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input"   type="text" placeholder="@lang('admin.username')" name="username" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="@lang('admin.password')" name="password" autocomplete="new-password">
								</div>
								<div class="row m-login__form-sub">

									<div class="col m--align-right m-login__form-right">
										<a href="javascript:;" id="m_login_forget_password" class="m-link" onclick="go_to_password_recovery()">
											@lang('admin.forgotten_password')
										</a>
									</div>
								</div>
								<div class="m-login__form-action">
									<button onclick="alert_email_sent_close()" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
										@lang('admin.login')
									</button>
								</div>
							</form>
						</div>

						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">
									@lang('admin.forgotten_password')
								</h3>
								<div class="m-login__desc" style="color: #fff;">
									@lang('admin.enter_email_to_reset_password')
								</div>
							</div>

							<div id="alert_email_not_found" style="display:none" class="alert alert-danger alert-dismissible fade show" role="alert">
								<button onclick="alert_email_not_found_close()" type="button" class="close" aria-label="Close"></button>
								<strong>
									@lang('admin.email_not_found')
								</strong>
							</div>
							<form class="m-login__form m-form" id="email_recovery_form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="email" placeholder="@lang('admin.email')" name="email" id="m_email_custom" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<a href="#" id="password_recovery_submit_button" onclick="submitPasswordRecoveryRequest()" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
										@lang('admin.request')
									</a>
									&nbsp;&nbsp;
									<button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn" onclick="back_to_login()">
										@lang('admin.cancel')
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="/admin_theme/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="/admin_theme/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Base Scripts -->
        <!--begin::Page Snippets -->
		<script src="/admin_theme/assets/snippets/pages/user/login.js" type="text/javascript"></script>
		<!--end::Page Snippets -->



		<script type="text/javascript">
			$(document).ready(function(){
				$("#m_email_custom").keydown(function(event) {
				    if (event.keyCode === 13) {
				        $("#password_recovery_submit_button").click();
						event.preventDefault();
						event.stopPropagation();
				    }
				});
			});

			var login =  $('#m_login');
			var btn = $("#password_recovery_submit_button");
			var form = $("#email_recovery_form");

			function displaySignInForm() {
				login.removeClass('m-login--forget-password');
				login.removeClass('m-login--signup');

				login.addClass('m-login--signin');
				login.find('.m-login__signin').animateClass('flipInX animated');

				var signInForm = login.find('.m-login__signin form');
				signInForm.clearForm();
				signInForm.validate().resetForm();
			}

			function submitPasswordRecoveryRequest(){


				form.validate({
					rules: {
						email: {
							required: true,
							email: true
						}
					}
				});

				if (!form.valid()) {
					return;
				}

				btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
				$('#alert_email_not_found').hide();

				form.ajaxSubmit({
					url: '{{url('password_recovery/request')}}',
					method: 'POST',
					success: function(response, status, xhr, form) {
						setTimeout(function() {
							form.clearForm(); // clear form
							form.validate().resetForm(); // reset validation states

							if(response == 'success:email_found'){
								displaySignInForm();
								$('#alert_email_sent').show();
							};
							if(response == 'error:email_not_found'){
								$('#alert_email_not_found').show();
							}
						}, 500);

					},
					error: function(response, status, xhr, form){
						alert('@lang('admin.cant_connect_to_database')');
					},
					complete: function() {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
					}
				});
			}

			function alert_email_sent_close() {
				$('#alert_email_sent').hide();
			}
			function alert_email_not_found_close() {
				$('#alert_email_not_found').hide();
			}
			function back_to_login(){
                $('#alert_email_not_found').hide();
            }
            function go_to_password_recovery(){
				$('.close').click();
			}
            jQuery(document).ready(function(){
				@if (session()->has('notify'))
                displayNotification('{{ isset(session()->get('notify')['message']) ? session()->get('notify')['message'] : session()->get('notify') }}',
                    '{{ isset(session()->get('notify')['type']) ? session()->get('notify')['type'] : 'success' }}');
				@php(session()->forget('notify'))
				@endif
            });


            function displayNotification(message,type){
                $.notify({
                    message: message+''
                }, {
                    type: type
                });
            }
		</script>
	</body>
	<!-- end::Body -->
</html>
