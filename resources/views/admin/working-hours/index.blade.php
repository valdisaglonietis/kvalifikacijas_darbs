@extends('admin.layout.main')

@section('content')
    <div class="m-portlet__head" style="padding-top:30px">
        <div class="m-portlet__head-tools row padding-top">
            <div class="row">
                <div class="form-group m-form__group col-md-3">
                </div>

                <div class="form-group m-form__group col-md-3 offset-md-6">
                    <a class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success bottom-right" href="{{url($url.'edit')}}" ><i class="fa fa-check-circle"></i> @lang('admin.add_new') </a>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div id="calendar"></div>
    </div>
@endsection

@section('css')
    <link href="/admin_theme/additional/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="/fullcalendar-scheduler-1.9.3/scheduler.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .calendar_param + span > span > span > span.select2-selection__rendered {
            text-align: left;
        }
    </style>
@endsection

@section('scripts')
   <script src="/admin_theme/additional/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/fullcalendar-scheduler-1.9.3/scheduler.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var workHoursCalendar = $('#calendar');
        var roomCount = 0;
        $(document).ready(function() {
            workHoursCalendar.fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                groupByDateAndResource: true,
                allDaySlot: false,
                minTime: '07:00:00',
                maxTime: '22:00:00',
                height: 'auto',
                firstDay: 1,
                fixedWeekCount: false,
                defaultView: 'roomsWeek',
                slotMinutes: 30,
                timeFormat: 'HH:mm',
                titleFormat: 'D. MMMM, Y',
                defaultEventMinutes: 60,
                resourceLabelText: '@lang('admin.room')',
                axisFormat: 'HH:mm',
                agenda: 'HH:mm',
                selectable: true,
                editable: false,
                nowIndicator: true,
                slotEventOverlap: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'roomsDay,roomsWeek,timelineMonth'
                },

                views: {

                    roomsDay: {
                        type: 'agenda',
                        slotLabelFormat: [
                            'dddd,D',
                            'HH:mm'
                        ],
                    },
                    roomsWeek: {
                        type: 'agenda',
                        duration: {
                            days: 7
                        },
                        firstDay: 1,
                        slotLabelFormat: [
                            'ddd, D',
                            'HH:mm',
                        ],
                        columnHeaderFormat: 'dddd, DD. MMMM'
                    },
                    timelineMonth: {
                        slotLabelFormat: [
                            'dddd, DD. MMMM',
                        ]
                    }
                },

                resources: function(callback) {
                    $.ajax({
                        type: 'GET',
                        url: '{{url('rooms/list')}}',
                        success: function(resources) {
                            resources = JSON.parse(resources);
                            roomCount = resources.length;
                            resources.forEach(function(resource, index) {
                                resources[index].title = resource.name;
                            });
                            callback(resources);
                        }
                    });
                },

                events: function(start, end,timezone, callback) {
                    $.ajax({
                        type: 'GET',
                        url: '{{url('working-hours/list')}}',
                        data: {
                            start: 				start.format('YYYY-MM-DD'),
                            end: 				end.format('YYYY-MM-DD')
                        },
                        success: function(events) {
                            callback(JSON.parse(events));
                        }
                    });
                },
                eventAfterAllRender: function(view) {
                    $(".fc-view.fc-roomsDay-view.fc-agenda-view").css("width",120*roomCount);
                    $(".fc-view.fc-roomsWeek-view.fc-agenda-view").css("width",7*120*roomCount);
                },

                buttonText: {
                    month: '@lang('admin.month')',
                    today: '@lang('admin.today')',
                    listWeek: '@lang('admin.list')',
                    roomsDay: '@lang('admin.day')',
                    roomsWeek: '@lang('admin.week')',
                },
                monthNames: [
                    'Janvāris',
                    'Februāris',
                    'Marts',
                    'Aprīlis',
                    'Maijs',
                    'Jūnijs',
                    'Jūlijs',
                    'Augusts',
                    'Septembris',
                    'Oktobris',
                    'Novemberis',
                    'Decemberis'
                ],
                monthNamesShort: [
                    'Janvāris',
                    'Februāris',
                    'Marts',
                    'Aprīlis',
                    'Maijs',
                    'Jūnijs',
                    'Jūlijs',
                    'Augusts',
                    'Septembris',
                    'Oktobris',
                    'Novemberis',
                    'Decemberis'
                ],
                dayNames: [
                    'Svētdiena',
                    'Pirmdiena',
                    'Otrdiena',
                    'Trešdiena',
                    'Ceturtdiena',
                    'Piektdiena',
                    'Sestdiena'
                ],
                dayNamesShort: [
                    'Sv',
                    'Pr',
                    'Ot',
                    'Tr',
                    'Ce',
                    'Pk',
                    'Se'
                ]
            });
        });
    </script>
@endsection
