@extends('admin.layout.main')

@section('content')

    <div class="m-portlet__body">
        <form id="form_general" name="form_general" method="post" action="" class="form-horizontal"
              enctype="multipart/form-data">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            @lang('admin.'.$title):
                            @if($intervalHours->id)
                                @lang('admin.editing')
                            @else
                                @lang('admin.adding')
                            @endif
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <button type="submit"  class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success">
                                <i class="fa fa-check-circle"></i> @lang('admin.save_and_back')</button>
                        </li>
                        <li class="m-portlet__nav-item">
                            <a href="{{ url($url) }}"
                               class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                        </li>
                        @if($intervalHours->id)
                        <li class="m-portlet__nav-item">
                            <a onclick="return confirm('@lang('admin.confirm')');" href="{{ url($url.'delete/'.$intervalHours->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger"><i class="fa fa-times"></i> @lang('admin.delete-all-day')</a>
                        </li>
                        <li class="m-portlet__nav-item">
                            <a onclick="return confirm('@lang('admin.confirm')');" href="{{ url($url.'delete-all/'.$intervalHours->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger"><i class="fa fa-times"></i> @lang('admin.delete-all-intervals')</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="form-row-seperated">
                    <div class="form-group m-form__group row ">
                    </div>
                </div>
                <div class="form-row-seperated">
                    <div class="form-group m-form__group row ">
                    </div>
                </div>
                <div class="form-row-seperated">
                    <div class="form-group m-form__group row ">
                    </div>
                </div>
                <div class="form-row-seperated">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label"
                               style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.doctor')
                            :*</label>
                        <div class="col-md-9">
                            <select class="form-control m-input" id="param_doctor" name="doctor">
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label"
                               style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.room')
                            :*</label>
                        <div class="col-md-9">
                            <select class="form-control m-input" id="param_room" name="room">
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label"
                               style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.date_from')
                            :*</label>
                        <div class="col-md-9">
                            <input required="" type="text" class="form-control datepicker" style="width: 100%;"
                                   id="date_from" name="date_from" value="{{ Input::old('date_from',($intervalHours->start?date('d.m.Y',strtotime($intervalHours->start)):date('d.m.Y'))) }}"
                                   placeholder="">
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label"
                               style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.date_to')
                            :*</label>
                        <div class="col-md-9">
                            <input required="" type="text" class="form-control datepicker" style="width: 100%;"
                                   id="date_to" name="date_to" value="{{Input::old('date_to',($intervalHours->end?date('d.m.Y',strtotime($intervalHours->end)):date('d.m.Y'))) }}" placeholder="">
                        </div>
                    </div>
                    @php($weekdays = (Input::old('weekday',0))? array_keys(Input::old('weekday')):($globalHours->day_numbers?explode(',',$globalHours->day_numbers):[]))
                    <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.weekday'):*</label>
                        <div class="col-md-9">
                            @for($i = 0; $i < 7; $i++)
                                <div class="form-group m-form__group row">
                                    <label class="col-sm-2 col-form-label" style="">
                                        <input name="weekday[{{$i}}]" {{in_array($i,$weekdays)?'selected="selected" checked=""':''}} type="checkbox"> @lang('admin.'.$weekDayNames[$i])
                                    </label>
                                    <input required="" type="text" class="form-control timepicker col-sm-2 offset-sm-2"
                                           style="width: 100%;" id="time_from[{{$i}}]" name="time_from[{{$i}}]"
                                           value="{{ ((Input::old('time_from')[$i])?Input::old('time_from')[$i]:((isset($daysTimeFrom[$i])&& $daysTimeFrom[$i])?$daysTimeFrom[$i]:"9:00")) }}" placeholder="">
                                    <input required="" type="text" class="form-control timepicker col-sm-2"
                                           style="width: 100%;" id="time_to[{{$i}}]" name="time_to[{{$i}}]"
                                           value="{{ ((Input::old('time_to')[$i])?Input::old('time_to')[$i]:((isset($daysTimeTo[$i])&& $daysTimeTo[$i])?$daysTimeTo[$i]:"18:00")) }}" placeholder="">
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>



@endsection

@section('css')
@endsection

@section('scripts')
    @php
        // old doctor select
        $oldDoctor = Input::old('doctor',$globalHours->user_id);
        if(strlen($oldDoctor) && is_numeric($oldDoctor) && $oldDoctor != '0'){
            $doctor = \App\Models\Users::find($oldDoctor);
        }
        // old room select
        $oldRoom = Input::old('room',$globalHours->room_id);
        if(strlen($oldRoom) && is_numeric($oldRoom) && $oldRoom != '0'){
            $room = \App\Models\Rooms::find($oldRoom);
        }
    @endphp

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
            autoclose: true,
            weekStart: 1
        });

        $('.timepicker').timepicker({
            minuteStep: 5,
            appendWidgetTo: 'body',
            showMeridian: false,
            autoclose: true
        });
        var select2_doctor = $('#param_doctor').select2({
            allowClear: true,
            placeholder: "@lang('admin.none_selected')",
            ajax: {
                method: 'get',
                url: '{{url('users/list')}}',
                delay: 250,
                dataType: 'json',
                processResults: function (response) {
                    return {
                        results: response
                    };
                }
            }
        });



        var select2_room = $('#param_room').select2({
            allowClear: true,
            placeholder: "@lang('admin.none_selected')",
            ajax: {
                method: 'get',
                url: '{{url('rooms/list')}}',
                delay: 250,
                dataType: 'json',
                processResults: function (response) {
                    return {
                        results: response
                    };
                }
            },
        });

        @if(isset($doctor) && $doctor)
            var option = new Option('{{$doctor->full_name}}','{{$doctor->id}}',true,true);
        @else
            var option = new Option('---------','0',true,true);
        @endif
        select2_doctor.append(option).trigger('change');

        @if(isset($room) && $room)
            var option = new Option('{{$room->name}}','{{$room->id}}',true,true);
        @else
            var option = new Option('---------','0',true,true);
        @endif
        select2_room.append(option).trigger('change');
    </script>
@endsection


