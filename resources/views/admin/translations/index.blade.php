@extends('admin.layout.main')

@section('styles')
@endsection

@section('content')

    <div class="m-portlet__body">
        <div class="m-form m-form--label-align-right  m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-7 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="@lang('admin.search')..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xl-5 order-1 order-xl-2 m--align-right">
                    <button class="btn btn-focus m-btn m-btn--icon m-btn--air m-btn--pill btn-success" type="submit" form="form_general" value="Submit">
                        <span><i class="fa fa-plus"></i><span>@lang('admin.save')</span></span>
                    </button>
                    @if(Auth::user()->super_admin)
                    <a href="{{ url($url.'add') }}" class="btn btn-focus m-btn m-btn--icon m-btn--air m-btn--pill btn-warning" onclick="return confirm('@lang('admin.confirm')');">
                        <span><i class="fa fa-plus"></i><span>@lang('admin.add_new')</span></span>
                    </a>
                    @endif
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>

            </div>
        </div>

        <table class="" cellspacing="0" width="100%" id="mydatatable_translations">
        </table>
    </div>

    <form action="" method="POST" id="form_general" style="display:none">
    </form>

@endsection

@section('css')
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            var datatable_translations = $('#mydatatable_translations').mDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'GET',
                            url: '{{url($url.'list')}}',
                        }
                    },
                    pageSize: 10,
                },
                // pagination: true,
                searchDelay: 4000,
                search: {
                    input: $('#generalSearch')
                },
                infoEmpty: '@lang('admin.no_records')',
                layout: {
                    theme: 'default', // datatable theme
                    class: '' // custom wrapper class
                },

                columns: [
                    {
                        title: '#',
                        field: 'key',
                    },
                    @foreach(config('application.languages') as $language)
                    {
                        field: "{{$language}}",
                        title: "{{$language}}",
                        template: function (row) {
                            return  '<input type="text" class="input-text validate[required] form-control inputField" name="translations[{{$language}}]['+row.key+']"\
                            value="'+row{{'.'.$language}}+'"  language="{{$language}}" key="'+row.key+'" onchange="addTranslation(this.id,this.value,this.name)" id="translations[{{$language}}]['+row.key+']"/>';
                        },
                    },
                    @endforeach

                ],

                translate: {
                    records: {
                        processing: '@lang('admin.processing')...',
                        noRecords: '@lang('admin.no_records')'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                default: {
                                    // first: 'Primero',
                                    // prev: 'Anterior',
                                    // next: 'Siguiente',
                                    // last: 'Último',
                                    // more: 'Más páginas',
                                    // input: 'Número de página',
                                    // select: 'Seleccionar tamaño de página'
                                },
                                info: '@lang('admin.displaying_x_records_from_n_records')'
                            }
                        }
                    }
                }
            });
        });

        function addTranslation(id,value,name) {
            console.log("id: "+id+" value: "+value+" name: "+name);
            var element = $('#form_general #'+id).first();
            console.log(element.length);
            if (element.length) {
                element.attr("value",this.value);
            }else{
                console.log('new');
                $('#form_general').append('<input type="text" value="'+value+'" name="'+name+'" id="'+id+'">');
            }
        }
    </script>
@endsection
