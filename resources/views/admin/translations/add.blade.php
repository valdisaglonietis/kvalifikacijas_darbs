@extends('admin.layout.main')

@section('content')
    <form id="form_general" name="form_general" method="post" action="" class="form-horizontal">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                            @lang('admin.'.$title): @lang('admin.adding_new')
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"  type="submit" form="form_general" value="Submit"><i class="fa fa-check-circle"></i> @lang('admin.add_new') </button>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="m-portlet__body">

            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label text-center" for="language">@lang('admin.key')*:</label>
                <div class="col-10">
                    <input required type="text" class="form-control" name="key" value="{{Input::old('key')}}"
                           placeholder="" id="key" maxlength="255" oninvalid="this.setCustomValidity('@lang('admin.field_is_required')')"
                           oninput="setCustomValidity('')">
                </div>
            </div>
            @foreach(config('application.languages') as $language)
                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label text-center" for="language">@lang('admin.'.$language)*:</label>
                    <div class="col-10">
                        <input type="text" class="form-control" name="{{$language}}" value="{{Input::old($language)}}"
                               placeholder="" id="first_name" maxlength="255">
                    </div>
                </div>
            @endforeach
        </div>
    </form>

@endsection

@section('scripts')

@endsection

@section('css')
@endsection
