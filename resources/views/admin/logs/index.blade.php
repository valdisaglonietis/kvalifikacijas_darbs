@extends('admin.layout.main')

@section('content')
    <!-- Main Content Starts -->
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-7 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="@lang('admin.search')..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                        <span><i class="la la-search"></i></span>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin::Section-->
                    <div class="m-section">
                        <div class="m-section__content">
                            <table class="" cellspacing="0" width="100%" id="mydatatable_logs">
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>

    <!-- Main Content Ends -->
@endsection

@section('css')
@endsection

@section('scripts')
    <script type="text/javascript">

        function checkNumber($number){
            var regex = /[0-9]/;
            return regex.test($number);
        }

        var datatable_logs = $('#mydatatable_logs').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: '{{url($url.'list')}}',
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            order: [
                [ 0, "desc"]
            ],
            aaSorting : [[0,"desc"]],
            bDestroy :true,
            pagination: true,
            infoEmpty: '@lang('admin.no_records')',
            searchDelay: 4000,
            search: {
                input: $('#generalSearch')
            },
            layout: {
                theme: 'default', // datatable theme
                class: '' // custom wrapper class
            },

            columns: [
                {
                    title: '@lang('admin.user')',
                    field: 'user'
                }, {
                    title: '@lang('admin.action')',
                    field: 'action'
                }, {
                    title: '@lang('admin.module')',
                    field: 'module'
                }, {
                    title: '@lang('admin.object')',
                    field: 'object'
                }, {
                    title: '@lang('admin.additional-info')',
                    field: 'additional_info'
                }, {
                    title: '@lang('admin.time')',
                    field: 'time'
                }
            ],

            translate: {
                records: {
                    processing: '@lang('admin.processing')...',
                    noRecords: '@lang('admin.no_records')'
                },
                toolbar: {
                    pagination: {
                        items: {
                            info: '@lang('admin.displaying_x_records_from_n_records')'
                        }
                    }
                }
            }
        });
    </script>
@endsection