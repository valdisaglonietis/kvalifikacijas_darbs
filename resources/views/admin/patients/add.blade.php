@extends('admin.layout.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Main content -->
            <div class="portlet">
                <form method="post" action="" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    @lang('admin.'.$title): @lang('admin.adding')
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="actions btn-set">
                                <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save_and_continue')</button>
                                <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-body form">
                            <div class="form-row-seperated">

                                <div class="form-group row">
                                    <label class="col-md-2 control-label">@lang('admin.first-name'):<span class="required">*</span></label>
                                    <div class="col-md-10">
                                        <input required type="text" class="form-control" name="first-name" value="{{Input::old('first-name')}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 control-label">@lang('admin.last-name'):<span class="required">*</span></label>
                                    <div class="col-md-10">
                                        <input required type="text" class="form-control" name="last-name" value="{{Input::old('last-name')}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 control-label">@lang('admin.phone'):<span class="required">*</span></label>
                                    <div class="col-md-10">
                                        <input required type="text" class="form-control" name="phone" value="{{Input::old('phone')}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 control-label">@lang('admin.email'):</label>
                                    <div class="col-md-10">
                                        <input type="email" class="form-control" name="email" value="{{Input::old('email')}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 control-label">@lang('admin.sex'):</label>
                                    <div class="col-md-10">
                                        <select name="sex" class="form-control m-input" autocomplete="off">
                                            @foreach(config('helpers.sex_types') as $key => $type)
                                                <option value="{{$key}}" {{Input::old('sex') == $key ? 'selected':''}}>
                                                    @lang('admin.'.$type)
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Main content ends -->
        </div>
    </div>
@endsection
@section('scripts')
@endsection
@section('css')
@endsection