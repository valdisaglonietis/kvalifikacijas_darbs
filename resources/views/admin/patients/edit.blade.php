@extends('admin.layout.main')

@section('content')
<form id="form_general" name="form_general" method="post" action="" class="form-horizontal m-form m-form--state" enctype="multipart/form-data">
    <div class="m-portlet__body">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        @lang('admin.patient'): {{$patient->full_name}}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save_and_back')</button>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a onclick="return confirm('@lang('admin.confirm')');" href="{{ url($url.'delete/'.$patient->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger"><i class="fa fa-times"></i> @lang('admin.delete')</a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#m_tabs_1_1">@lang('admin.main')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_3">@lang('admin.history-of-health')</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active show" id="m_tabs_1_1" role="tabpanel">
                <div class="m-portlet__body">
                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.first-name'):<span class="required">*</span></label>
                        <div class="col-md-10">
                            <input required type="text" class="form-control" name="first-name" value="{{Input::old('first-name',$patient->first_name)}}" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.last-name'):<span class="required">*</span></label>
                        <div class="col-md-10">
                            <input required type="text" class="form-control" name="last-name" value="{{Input::old('last-name',$patient->last_name)}}" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.phone'):<span class="required">*</span></label>
                        <div class="col-md-10">
                            <input required type="text" class="form-control" name="phone" value="{{Input::old('phone',$patient->phone)}}" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.email'):</label>
                        <div class="col-md-10">
                            <input type="email" class="form-control" name="email" value="{{Input::old('email',$patient->email)}}" placeholder="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 control-label">@lang('admin.sex'):</label>
                        <div class="col-md-10">
                            <select name="sex" class="form-control m-input" autocomplete="off">
                                @foreach(config('helpers.sex_types') as $key => $type)
                                    <option value="{{$key}}" {{Input::old('sex',$patient->sex) == $key ? 'selected':''}}>
                                        @lang('admin.'.$type)
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_tabs_1_3" role="tabpanel">
                @include($view.'partials.history_of_health')
            </div>
        </div>
    </div>
</form>
@endsection

@section('css')

@endsection

@section('scripts')
    <script type="text/javascript">
        datatable_history_of_health.init();
    </script>
@endsection


