<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
    <div class="row align-items-center">
        <div class="col-xl-7 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
                <div class="col-md-4">
                    <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="@lang('admin.search')..." id="healthHistorySearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-section">
    <div class="m-section__content">
        <table class="" cellspacing="0" width="100%" id="mydatatable_history_of_health">
        </table>
    </div>
</div>

<script type="text/javascript">

    var datatable_history_of_health = {
        init:   function() {
            $('#mydatatable_history_of_health').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'GET',
                            url: '{{url($url.'health-history/'.$patient->id)}}',
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                order: [
                    [ 0, "desc"]
                ],
                aaSorting : [[0,"desc"]],
                bDestroy :true,
                pagination: true,
                infoEmpty: '@lang('admin.no_records')',
                searchDelay: 4000,
                search: {
                    input: $('#healthHistorySearch')
                },
                layout: {
                    theme: 'default', // datatable theme
                    class: '' // custom wrapper class
                },

                columns: [
                    {
                        field: 'id',
                        title: 'id'
                    }, {
                        field: 'start',
                        title: '@lang('admin.planned_time')'
                    }, {
                        field: 'doctor',
                        title: '@lang('admin.doctor')'
                    }, {
                        field: 'room',
                        title: '@lang('admin.room')'
                    }
                ],

                translate: {
                    records: {
                        processing: '@lang('admin.processing')...',
                        noRecords: '@lang('admin.no_records')'
                    },
                    toolbar: {
                        pagination: {
                            items: {

                                info: '@lang('admin.displaying_x_records_from_n_records')'
                            }
                        }
                    }
                }
            });
        }
    };
</script>
