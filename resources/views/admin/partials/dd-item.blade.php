@foreach($items as $item)
    <li class="dd-item dd3-item dd-item-custom" data-id="{{ $item->id }}">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
            {{--TODO change this link into a standard too--}}
            <a href="{{url("$module_url/edit/$item->id")}}">{{$item->key}} </a>


            <div class="pull-right">
                <input type="checkbox" name="status" value="{{$item->id}}" {{ Input::old('status', $item->status) == 1 ? 'checked' : '' }} class="make-switch" data-on-color="success" data-off-color="default" data-on-text="@lang('admin.active')" data-off-text="@lang('admin.inactive')">
            </div>
        </div>
        @if($item->children)
            <ol class="dd-list">
                @include('admin.partials.dd-item',['items' => $item->children,'module_url'=>$module_url])
            </ol>
        @endif
    </li>
@endforeach