<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                {{ isset($title) ? Lang::get('admin.'.$title) : Lang::get('admin.'.Request::segment(1)) }}
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>

                @if(isset($breadcrumbs))
                    @php
                        $size = count($breadcrumbs);
                        $current = 0;
                    @endphp

                    @foreach($breadcrumbs as $breadcrumb)
                        @php($current++)
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{(isset($breadcrumb['link']) && !($size <= $current))?$breadcrumb['link']:'#'}}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    @if(isset($breadcrumb['title']))
                                        @lang('admin.'.$breadcrumb['title'])
                                    @else
                                        @lang('admin.'.$breadcrumb)
                                    @endif
                                </span>
                            </a>
                        </li>
                    @endforeach

                @endif

                @if(isset($breadcrumbs_title) || isset($action))
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                {{isset($action)?Lang::get('admin.'.$action):$breadcrumbs_title}}
                            </span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>