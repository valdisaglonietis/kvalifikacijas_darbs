<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<!-- begin::global::meta-->
		<title>
			@lang('admin.main_title')
		</title>
		<meta charset="utf-8" />
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- end::global::meta-->
		<!--begin::global::style -->
		<link href="/admin_theme/additional/webfont/default_local.min.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/additional/custom/style.css?v=4" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="/admin_theme/assets/demo/default/media/img/logo/glc-logo-rgb_burned.png" />
		<!--end::global::style -->
		<!--begin::page::style-->
		@yield('css')
		<!--end::page::style-->


	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	{{--m-aside-left--fixed  ERROR, should add because it would be more convenient if the sidebar was seperate from the page--}}

	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark  m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- BEGIN: Header -->
			<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="{{url('')}}" class="m-brand__logo-wrapper">
										<img style="width:159px;height: 69px" src="/media/default/logo.svg">
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
					 ">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>
						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button>
							<div style="max-width: 80%;" id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >
							</div>
							<!-- END: Horizontal Menu -->
							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">

										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="{{url((isset(Auth::user()->photo) && Auth::user()->photo)?'/media/user_images/'.Auth::user()->photo:'/media/default/user_images.jpg')}}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
												</span>
												<span class="m-topbar__username m--hide">
													{{ Auth::user()->first_name.' '.Auth::user()->last_name }}
												</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url(/admin_theme/assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__pic">
																<img src="{{url((isset(Auth::user()->photo) && Auth::user()->photo)?'/media/user_images/'.Auth::user()->photo:'/media/default/user_images.jpg')}}" class="m--img-rounded m--marginless" alt=""/>

															</div>
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500">
																	{{ Auth::user()->first_name.' '.Auth::user()->last_name }}
																</span>
																<a href="" class="m-card-user__email m--font-weight-300 m-link">
																	{{ Auth::user()->email }}
																</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																	</span>
																</li>
																<li class="m-nav__item">
																	<a href="{{url('users/profile')}}" class="m-nav__link">
																		<i class="m-nav__link-icon flaticon-profile-1"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">
																					@lang('admin.my-profile')																			</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__separator m-nav__separator--fit"></li>
																<li class="m-nav__item">
																	<a href="/login/logout" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																		@lang('admin.logout')
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			<!-- END: Header -->
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="/" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												@lang('admin.dashboard')
											</span>
										</span>
									</span>
								</a>
							</li>
							@if(hasPermissions('calendar','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'calendar' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('calendar') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-calendar"></i>
									<span class="m-menu__link-text ">
											@lang('admin.calendar')
										</span>
								</a>
							</li>
							@endif
							@if(hasPermissions('working-hours','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'working-hours' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('working-hours') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-calendar-2"></i>
									<span class="m-menu__link-text ">
											@lang('admin.working-hours')
										</span>
								</a>
							</li>
							@endif
							@if(hasPermissions('patients','view',0))
								<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'patients' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									<a  href="{{ url('patients') }}" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-icon flaticon-users"></i>
										<span class="m-menu__link-text ">
											@lang('admin.patients')
										</span>
									</a>
								</li>
							@endif
							@if(hasPermissions('rooms','view',0))
								<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'rooms' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									<a  href="{{ url('rooms') }}" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-icon flaticon-app"></i>
										<span class="m-menu__link-text ">
											@lang('admin.rooms')
										</span>
									</a>
								</li>
							@endif
							@if(hasPermissions('users','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'users' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('users') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-users"></i>
									<span class="m-menu__link-text ">
											@lang('admin.users')
										</span>
								</a>
							</li>
							@endif
							@if(hasPermissions('user-groups','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'user-groups' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('user-groups') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-map"></i>
									<span class="m-menu__link-text ">
											@lang('admin.user-groups')
										</span>
								</a>
							</li>
							@endif

							@if(hasPermissions('translations','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'translations' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('translations') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-speech-bubble"></i>
									<span class="m-menu__link-text ">
											@lang('admin.translations')
										</span>
								</a>
							</li>
							@endif
							@if(hasPermissions('logs','view',0))
							<li class="m-menu__item  m-menu__item--submenu {{Request::segment(1) == 'logs' ? 'm-menu__item--open m-menu__item--expanded':''}}" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="{{ url('logs') }}" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-clipboard"></i>
									<span class="m-menu__link-text ">
											@lang('admin.logs')
									</span>
								</a>
							</li>
							@endif
						</ul>
					</div>
					<!-- END: Aside Menu -->
				</div>
				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
                    @include('admin.partials.breadcrumb')

					<div class="m-content">
						@if(isset($errors) && !empty($errors))
							<?php $errors = $errors->all(); ?>
							@if(!empty($errors))
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
								<p>@lang('admin.error'):</p>
								@foreach($errors as $e)
									<p><strong>{{ $e }}</strong></p>
								@endforeach
							</div>
							@endif
						@endif
						<div class="m-portlet m-portlet--full-height">
			            	@yield('content')
						</div>
					</div>
				</div>
			</div>
			<!-- end:: Body -->
			<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">

						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
											<span class="m-footer__copyright">

												{{--2008 - {{ date("Y")}} --}}
												&copy; Valda Aglonieša Kvalifikācijas darbs 2018
												{{--Izstrādātājs: <a href="http://www.e-r.lv" target="_blank" style="color:#94C11F;">SIA e-risinājumi</a>--}}
											</span>
								</div>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"><i class="la la-arrow-up"></i></div>
		<!-- end::Page -->
		<!--begin::global::scripts-->
		<script href="/admin_theme/additional/jquery/jquery.mousewheel.min.js" rel="stylesheet" type="text/javascript"></script>
		<script src="/admin_theme/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="/admin_theme/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="/admin_theme/assets/app/js/dashboard.js" type="text/javascript"></script>
		<!--end::global::scripts-->
        <!--begin::page::scripts-->
		@yield('scripts')
        <!--begin::page::scripts-->
		<script type="text/javascript">
            if(typeof GenericDataTable !== 'undefined'){
                GenericDataTable.init();
                // GenericDataTable.init();
            }

            $('form').submit(function (e) {
                $(this).closest('form').find('input:valid, textarea:valid').removeClass('not-valid');
                $(this).closest('form').find('input:invalid, textarea:invalid').addClass('not-valid');
                $(this).closest('form').find('input:invalid, textarea:invalid').first().focus();
            });

            $('button[type="submit"]').click(function (e) {
				$(this).closest('form').find('input:valid, textarea:valid').removeClass('not-valid');
				$(this).closest('form').find('input:invalid, textarea:invalid').addClass('not-valid');
                $(this).closest('form').find('input:invalid, textarea:invalid').first().focus();
            });

			jQuery(document).ready(function(){
				@if (session()->has('notify'))
				displayNotification('{{ isset(session()->get('notify')['message']) ? session()->get('notify')['message'] : session()->get('notify') }}',
					'{{ isset(session()->get('notify')['type']) ? session()->get('notify')['type'] : 'success' }}');
				@php(session()->forget('notify'))
				@endif
			});


            function displayNotification(message,type){
                $.notify({
                    message: message+''
                }, {
                    type: type
                });
            }
            {{--$('input, select, textarea').on("invalid", function(e) {--}}
                {{--console.log(e.target.value);--}}
                {{--if(e.target.hasAttribute('required') && e.target.value === ''){--}}
                    {{--e.target.setCustomValidity('@lang('admin.this-field-is-required')');--}}
				{{--}else{--}}
                    {{--e.target.setCustomValidity('');--}}
				{{--}--}}
            {{--});--}}
		</script>
	</body>
	<!-- end::Body -->
</html>
