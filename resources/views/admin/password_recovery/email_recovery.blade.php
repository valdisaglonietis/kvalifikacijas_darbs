<div style="padding-left:10px;">
<h3>
    @lang('admin.password_recovery'):
</h3>
@lang('admin.click_on_link_to_change_password',['link' => \Lang::get('admin.recover_password')])
<h2><a target="_blank" href="{{url('/password_recovery/'.$user->recovery_token)}}">@lang('admin.recover_password')</a></h2>
<small>@lang('admin.will_expire'): {{$user->recovery_due_date}}</small>
</div>
