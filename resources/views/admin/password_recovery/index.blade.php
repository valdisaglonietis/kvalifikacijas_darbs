<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			@lang('admin.main_title')
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->
		<link href="/admin_theme/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/admin_theme/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="/admin_theme/assets/demo/default/media/img/logo/favicon.ico" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-color:#9aaed4;">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
                                <img style="color:#9aaed4; width:150px;" src="/admin_theme/assets/app/media/img/logos/image2vector.svg">
							</a>
                            <h1 style="color:#20468f;font-weight:800; letter-spacing:3px; font-size:30px;">ENDO</h1>
						</div>
						@if(isset($user) && $user)
							<div class="m-login__signin">
								<div class="m-login__head">
									<h3 class="m-login__title">
										@lang('admin.password_change')
									</h3>
									<div class="m-login__desc">
										@lang('admin.enter_password_to_change_to')
									</div>
								</div>
								<div id="alert_passwords_dont_match" style="display:none" class="alert alert-danger alert-dismissible fade show" role="alert">
									<button onclick="alert_passwords_dont_match_close()" type="button" class="close" aria-label="Close"></button>
									<strong>
										@lang('admin.passwords-dont-match')
									</strong>
								</div>

								<div id="alert_password_is_too_short" style="display:none" class="alert alert-danger alert-dismissible fade show" role="alert">
									<button onclick="alert_password_is_too_short_close()" type="button" class="close" aria-label="Close"></button>
									<strong>
										@lang('admin.password_is_too_short_standard_8')
									</strong>
								</div>

								<form class="m-login__form m-form" id="change_password_form" action="">
									<div class="form-group m-form__group">
										<input id="password" class="form-control m-input m-login__form-input--last" type="password" placeholder="@lang('admin.password')" name="password" required>
									</div>
									<div class="form-group m-form__group">
										<input id="confirm_password" class="form-control m-input m-login__form-input--last" type="password" placeholder="@lang('admin.confirm_password')" name="confirm_password" required>
									</div>
									<div class="m-login__form-action">
										<a href="#" id="password_recovery_submit_button" onclick="submitPasswordChangeRequest()" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
											@lang('admin.change_password')
										</a>
									</div>
								</form>
							</div>
						@else
							<div class="m-login__signin">
								<div class="m-login__head">
									<h3 class="m-login__title">
										@lang('admin.password_change')
									</h3>
									<div class="alert alert-danger alert-dismissible fade show" role="alert">
										<strong>
											@lang('admin.recovery-token-is-invalid')
										</strong>
									</div>
								</div>

							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="/admin_theme/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="/admin_theme/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Base Scripts -->
        <!--begin::Page Snippets -->
		<script src="/admin_theme/assets/snippets/pages/user/login.js" type="text/javascript"></script>
		<!--end::Page Snippets -->



		<script type="text/javascript">
			@if(isset($user) && $user)
				var form = $("#change_password_form");
				var login =  $('#m_login');
				var btn = $("#password_recovery_submit_button");

				function submitPasswordChangeRequest(){
					$password = $('#password').val();

					if($('#password').val() == $('#confirm_password').val()){
						if($password.length >= 8){
							btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

							form.ajaxSubmit({
								url: '{{url('password_recovery/'.$user->recovery_token)}}',
								method: 'POST',
								success: function(response, status, xhr, form) {
									setTimeout(function() {
										btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove
										if(response == 'success:password_changed'){
											location.href = "{{url('login')}}"
										}
									}, 500);
								},
								error: function(response, status, xhr, form){
									alert('@lang('admin.cant_connect_to_database')');
								}
							});
						}else{
							$('#alert_password_is_too_short').show();
							form.clearForm(); // clear form
							form.validate().resetForm(); // reset validation states
							return;
						}
					}else{
						$('#alert_passwords_dont_match').show();
						form.clearForm(); // clear form
						form.validate().resetForm(); // reset validation states
						return;
					}


				}

				function alert_password_changed_close() {
					$('#alert_password_changed').hide();
				}
				function alert_passwords_dont_match_close() {
					$('#alert_passwords_dont_match').hide();
				}

				function alert_password_is_too_short_close() {
					$('#alert_password_is_too_short').hide();
				}
			@endif
		</script>
	</body>
	<!-- end::Body -->
</html>
