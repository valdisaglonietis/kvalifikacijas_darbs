<script type="text/javascript">

    var datatable_user_logs = $('#userLogs_datatable').mDatatable({
        layout: {
            theme: 'default',
            class: ''
        },
        columns: [
            {
                field: 'created_at',
                title: '@lang('admin.time')'
            },
            {
                field: 'action',
                title: '@lang('admin.action')'
            }, {
                field: 'module',
                title: '@lang('admin.module')'
            },{
                field: 'extra_id',
                title: '@lang('admin.target_id')'
            }, {
                field: 'extra_text',
                title: '@lang('admin.additional-info')'
            }
        ],
        search: {
            input: $('#userLogs_search')
        },
        searchDelay: 4000,
        data: {
            type: 'remote',
            source: {
                read: {
                    url: '{{url($url.'logs/'.$user->id)}}',
                    method: 'GET'
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        translate: {
            records: {
                processing: '@lang('admin.processing')...',
                noRecords: '@lang('admin.no_records')'
            },
            toolbar: {
                pagination: {
                    items: {
                        info: '@lang('admin.displaying_x_records_from_n_records')'
                    }
                }
            }
        }
    });
</script>