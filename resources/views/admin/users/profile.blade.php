@extends('admin.layout.main')

@section('content')
    <form id="form_general" name="form_general" method="post" action="" class="form-horizontal m-form m-form--state" enctype="multipart/form-data">

        <div class="m-portlet__body">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            @lang('admin.my-profile'): {{$user->full_name}}
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save')</button>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#m_tabs_1_1">@lang('admin.main')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2">@lang('admin.logs')</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active show" id="m_tabs_1_1" role="tabpanel">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label" for="first_name" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.first_name'):</label>
                                    <div class="col-10">
                                        <input disabled required type="text" class="form-control" name="first_name" value="{{Input::old('first_name',$user->first_name)}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label" for="last_name" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.last_name'):</label>
                                    <div class="col-10">
                                        <input disabled required type="text" class="form-control" name="last_name" value="{{Input::old('last_name',$user->last_name)}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label" for="phone" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.phone'):</label>
                                    <div class="col-10">
                                        <input disabled type="text" class="form-control" name="phone" value="{{Input::old('phone',$user->phone)}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label" for="username" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.email'):</label>
                                    <div class="col-10">
                                        <input disabled required type="email" class="form-control" name="email" value="{{Input::old('email',$user->email)}}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="control-label col-md-2">@lang('admin.user_group'):</label>
                                    <div class="col-md-10">
                                        <select disabled required class="table-group-action-input form-control input-medium select2-multiple" name="userroles[]" multiple="multiple">
                                            @foreach($user_roles as $userrole)
                                                <option value="{{ $userrole->name }}" {{Input::old('userroles')?in_array($userrole->name,Input::old('userroles',[])):($user->hasRole($userrole->name))?'selected="selected"':''}}>
                                                    {{ $userrole->key }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label" for="language" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.language'):</label>

                                    <div class="col-10">
                                        <div class="m-form__group form-group">
                                            <div class="m-radio-inline">
                                                @foreach(Config::get('application.languages') as $key =>$language)
                                                    <label class="m-radio">
                                                        <input type="radio" name="changeLocale" value="{{$language}}" {{$user->locale == $language? 'checked="checked"':''}}>
                                                        <a href="{{url(Request::url().'?language='.$language)}}" class="m-nav__link">
                                                            <img src="{{url('language_flags/flag_'.$language.'.png')}}" class=" m--marginless m--img-centered" alt=""/>
                                                        </a>
                                                        <span></span>
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-form__group row">
                                    <label class="control-label col-md-2">@lang('admin.image'):</label>
                                    <div class="col-md-10">
                                        <div></div>
                                        <div class="custom-file">
                                            <input type="file" name="photo" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">
                                                @lang('admin.choose')
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    @if(!empty($user->photo))
                                        <img style="max-height: 100%; width:100%;" src="{{url($photoLocation.$user->photo)}}">
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="m_tabs_1_2" role="tabpanel">
                    @include($view.'logs.html')
                </div>
            </div>
        </div>

        @endsection

        @section('css')

        @endsection

        @section('scripts')
            @include($view.'logs.javascript')
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.select2-multiple').select2();
                });
            </script>
@endsection


