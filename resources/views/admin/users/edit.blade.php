@extends('admin.layout.main')

@section('content')
    <form id="form_general" name="form_general" method="post" action="" class="form-horizontal m-form m-form--state" enctype="multipart/form-data">

    <div class="m-portlet__body">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        @lang('admin.user'): {{$user->full_name}}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save_and_back')</button>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                    </li>
                    <li class="m-portlet__nav-item">
                        <a onclick="return confirm('@lang('admin.confirm')');" href="{{ url($url.'delete/'.$user->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger"><i class="fa fa-times"></i> @lang('admin.delete')</a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#m_tabs_1_1">@lang('admin.main')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2">@lang('admin.logs')</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active show" id="m_tabs_1_1" role="tabpanel">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label" for="first_name" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.first_name')*:</label>
                                <div class="col-10">
                                    <input required type="text" class="form-control" name="first_name" value="{{Input::old('first_name',$user->first_name)}}" placeholder="">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label" for="last_name" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.last_name')*:</label>
                                <div class="col-10">
                                    <input required type="text" class="form-control" name="last_name" value="{{Input::old('last_name',$user->last_name)}}" placeholder="">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label" for="phone" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.phone'):</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" name="phone" value="{{Input::old('phone',$user->phone)}}" placeholder="">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label" for="username" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.email'):*</label>
                                <div class="col-10">
                                    <input required type="email" class="form-control" name="email" value="{{Input::old('email',$user->email)}}" placeholder="">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label" for="username" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.password'):*</label>
                                <div class="col-10">
                                    <input {{!$user->id?'required':''}} type="password" class="form-control" id="password" name="password" value="" placeholder="" autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group m-form__group row " id="password-confirmation-row">
                                <label class="col-lg-2 col-form-label" for="username" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.password_confirmation'):*</label>
                                <div class="col-10">
                                    <input {{!$user->id?'required':''}} type="password" class="form-control form-control-danger m-input" id="password-confirmation" name="password_confirmation" value="" placeholder="" autocomplete="new-password" onInput="checkPassword(this)">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label class="control-label col-md-2">@lang('admin.user_group'):</label>
                                <div class="col-md-10">
                                    <select required class="table-group-action-input form-control input-medium select2-multiple" name="userroles[]" multiple="multiple">
                                        @foreach($user_roles as $userrole)
                                            <option value="{{ $userrole->name }}" {{Input::old('userroles')?in_array($userrole->name,Input::old('userroles',[])):($user->hasRole($userrole->name))?'selected="selected"':''}}>
                                                {{ $userrole->key }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            @if(Auth::user()->super_admin)
                                <div class="form-group m-form__group row">
                                    <label class="control-label col-md-2 ">@lang('admin.active'):</label>
                                    <div class="col-md-10">
                                     <span class="m-switch m-switch--outline m-switch--success">
                                            <label>
                                            <input name="active" type="checkbox" {{ Input::old('active', $user->active) == 1 ? 'checked="checked"' : '' }}>
                                            <span></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group row">
                                <label class="control-label col-md-2">@lang('admin.image'):</label>
                                <div class="col-md-10">
	                                <div></div>
									<div class="custom-file">
										<input type="file" name="photo" class="custom-file-input" id="customFile">
										<label class="custom-file-label" for="customFile">
                                            @lang('admin.choose')
                                        </label>
									</div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                @if(!empty($user->photo))
		                            <img style="max-height: 100%; width:100%;" src="{{url($photoLocation.$user->photo)}}">
								@endif
                            </div>
                            
                        </div>



                    </div>
                </div>
            </div>
            <div class="tab-pane" id="m_tabs_1_2" role="tabpanel">
                    @include($view.'logs.html')
            </div>
        </div>
    </div>

@endsection

@section('css')

@endsection

@section('scripts')
        @include($view.'logs.javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2-multiple').select2();
        });

        function checkPassword(input) {

            if (input.value != document.getElementById('password').value) {
                $('#password-confirmation-row').addClass('has-danger');
                {{--                input.setCustomValidity('@lang('admin.passwords_do_not_match')');--}}
            } else {
                $('#password-confirmation-row').removeClass('has-danger');
                // input is valid -- reset the error message
                // input.setCustomValidity('');
            }
        }
    </script>
@endsection


