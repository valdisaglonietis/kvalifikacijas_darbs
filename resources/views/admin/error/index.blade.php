@extends('admin.layout.main')

@section('content')
    <!-- Main Content Starts -->
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    @lang('admin.error')
                </h3>
            </div>
        </div>
    </div>

    <div class="m-portlet__body">
        <h3>{{isset($error)?$error:''}}</h3>
        <a href="{{ url('') }}" class="btn default btn-warning">@lang('admin.dashboard')</a>
        <a href="{{ url(URL::previous()) }}" class="btn default btn-metal">@lang('admin.back')</a>
    </div>

    <!-- Main Content Ends -->
@endsection

@section('css')
    <!-- Page Level CSS Start -->
    <!-- Page Level CSS End -->
@endsection

@section('scripts')
    <!-- Page Level Scripts Start -->
    <!-- Page Level Scripts Start -->
@endsection


