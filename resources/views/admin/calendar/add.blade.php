@extends('admin.layout.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Main content -->
            <div class="portlet">
                <form method="post" action="" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i class="flaticon-calendar"></i>
						</span>
                                <h3 class="m-portlet__head-text">
                                    @lang('admin.'.$title): @lang('admin.adding')
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="actions btn-set">
                                <button type="submit" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"><i class="fa fa-check-circle"></i> @lang('admin.save')</button>
                                <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-body form">
                            <div class="form-row-seperated">

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-3 col-form-label" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.date_and_time'):</label>
                                    <div class="col-md-9"><h1>{{$workingHours->start}}</h1></div>
                                </div>
                            </div>

                            <div class="form-row-seperated">

                                <div class="form-group m-form__group row">
                                    <label class="col-lg-3 col-form-label"
                                           style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.patient')
                                        :*</label>
                                    <div class="col-md-9">
                                        <select class="form-control m-input" id="param_patient" name="patient">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Main content ends -->
        </div>
    </div>
@endsection
@section('scripts')
     @php
        // old doctor select
        $oldPatient = Input::old('patient');
        if(strlen($oldPatient) && is_numeric($oldPatient) && $oldPatient != '0'){
            $patient = \App\Models\Patients::find($oldPatient);
        }
    @endphp

     <script type="text/javascript">
         var select2_patient = $('#param_patient').select2({
             allowClear: true,
             placeholder: "@lang('admin.none_selected')",
             ajax: {
                 method: 'get',
                 url: '{{url('patients/list')}}',
                 delay: 250,
                 dataType: 'json',
                 processResults: function (response) {
                     return {
                         results: response
                     };
                 }
             }
         });

         @if(isset($patient) && $patient)
            var option = new Option('{{$patient->full_name}}','{{$patient->id}}',true,true);
         @else
            var option = new Option('---------','0',true,true);
         @endif
         select2_patient.append(option).trigger('change');
     </script>
@endsection
@section('css')
@endsection