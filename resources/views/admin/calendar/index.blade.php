@extends('admin.layout.main')
@section('styles')
@endsection
@section('content')
    <div class="m-portlet__head">
        <div class="m-portlet__head-tools row padding-top">
            <div class="row">
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div id="calendar"></div>
    </div>
@endsection
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="/fullcalendar-scheduler-1.9.3/scheduler.min.css" rel="stylesheet" type="text/css" />
    <style>
        .add_investigation {
            opacity: 0.5 !important;
            background: #6cbb5f !important;
            border-radius: 0 !important;
            border: none !important;
        }

        .edit_investigation {
            opacity: 0.5 !important;
            background: #bb5e59 !important;
            border-radius: 0 !important;
            border: none !important;
        }

        .edit_investigation:hover {
            opacity: 0.9 !important;
            background: #bb5e59 !important;
        }

        .add_investigation:hover {
            opacity: 0.9 !important;
            background: #3cdf20 !important;
        }

        .fc-unthemed .fc-event.fc-start .fc-content:before, .fc-unthemed .fc-event-dot.fc-start .fc-content:before {
            background: none !important;
        }

        .tooltipevent {
            width: 300px;
            border: 1px solid #bbbbbb;
            border-radius: 5px;
            background: whitesmoke;
            position: absolute;
            z-index: 10001;
            padding: 5px;
            color: #000000;
        }

        .tooltipevent span {
            font-weight: 500;
        }

        .fc-listWeek-view .fc-list-item-title a {
            color: #fff !important;
        }

        .fc-agendaWeek-view .fc-time-grid-event .fc-content .fc-title {
            margin-left: -27px;
            margin-top: -9px;
            color: #000;
        }

        .fc-agendaDay-view .fc-time-grid-event .fc-content .fc-title {
            margin-top: -6px;
            margin-left: -20px;
        }

        .fc-month-view .fc-day-grid-event .fc-content .fc-title {
            margin-left: -20px;
            font-size: 10px;
        }

        .fc-view.fc-agendaDay-view.fc-agenda-view .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-short > div > div.fc-title,
        .fc-view.fc-agendaWeek-view.fc-agenda-view .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-short > div > div.fc-title{
            margin-left:0px;
            margin-top:0px;
        }
        .fc-view.fc-listWeek-view.fc-list-view.fc-widget-content tr.fc-list-item.add_investigation.fc-has-url {
            display:none;
        }
    </style>
@endsection
@section('scripts')
    {{--<script src="/admin_theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/fullcalendar-scheduler-1.9.3/scheduler.min.js" type="text/javascript"></script>

    <script src="/admin_theme/assets/demo/default/custom/components/calendar/list-view.js" type="text/javascript"></script>

    <script type="text/javascript">
        // Calendar data display starts
        var asignmentCalendar = $('#calendar');

        $(document).ready(function () {
            asignmentCalendar.fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                allDaySlot: false,
                minTime: '07:00:00',
                maxTime: '22:00:00',
                height: 'auto',
                firstDay: 1,
                fixedWeekCount: false,
                defaultView: 'roomsWeek',
                slotMinutes: 30,
                defaultEventMinutes: 60,
                resourceLabelText: '@lang('admin.room')',
                axisFormat: 'HH:mm',
                agenda: 'HH:mm',
                slotLabelFormat: 'HH:mm',
                timeFormat: 'HH:mm',
                displayEventEnd: true,
                displayEventTime: false,
                selectable: true,
                editable: false,
                nowIndicator: true,
                slotEventOverlap: false,
                titleFormat: 'DD. MMMM, YYYY',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'roomsDay,roomsWeek,month,listWeek'
                },
                views: {
                    month: {
                        eventTextColor: '#000',
                        columnFormat: 'dddd',
                    },
                    listWeek: {
                        eventTextColor: 'rgba(0,0,0,0)',
                        listDayAltFormat: 'DD. MMMM'
                    },
                    roomsDay: {
                        groupByDateAndResource: true,
                        type: 'agenda',
                        slotLabelFormat: [
                            'd.MMMM, D',
                            'H:mm'
                        ],
                    },
                    roomsWeek: {
                        groupByDateAndResource: true,
                        firstDay: 1,
                        type: 'agenda',
                        duration: {
                            days: 7
                        },
                        dateAlignment: "week",
                        slotLabelFormat: [
                            'd.MMMM, D',
                            'H:mm'
                        ],
                        columnHeaderFormat: 'dddd, D. MMMM'
                    },
                },
                resources: function(callback) {
                    $.ajax({
                        type: 'GET',
                        url: '{{url('rooms/list')}}',
                        success: function(resources) {
                            resources = JSON.parse(resources);
                            roomCount = resources.length;
                            resources.forEach(function(resource, index) {
                                resources[index].title = resource.name;
                            });
                            callback(resources);
                        }
                    });
                },
                events: function(start, end,timezone, callback) {
                    $.ajax({
                        type: 'GET',
                        url: '{{url('calendar/list')}}',
                        data: {
                            start: 				start.format('YYYY-MM-DD'),
                            end: 				end.format('YYYY-MM-DD'),
                            // doctor:             $("#param_doctor").val(),
                        },
                        success: function(events) {
                            callback(JSON.parse(events));
                        }
                    });
                },
                eventMouseover: function (calEvent, jsEvent) {
                    var tooltip = '<div class="tooltipevent">' + calEvent.description + '</div>';
                    var $tooltip = $(tooltip).appendTo('body');
                    var xOffset = 10;
                    var yOffset = 20;
                    $(this).mouseover(function (e) {
                        $(this).css('z-index', 10000);
                        $tooltip.fadeIn('500');
                        $tooltip.fadeTo('10', 1.9);
                    }).mousemove(function (e) {
                        var mousex = e.pageX + xOffset;
                        var mousey = e.pageY + yOffset;
                        var tooltipOffset = $('.tooltipevent').width();
                        if ((mousex + tooltipOffset) > $(window).width()) {
                            $('.tooltipevent').css({top: mousey, left: mousex - tooltipOffset - 10 })
                        }
                        else {
                            $('.tooltipevent').css({top: mousey, left: mousex})
                        }
                    });
                },
                eventMouseout: function (calEvent, jsEvent) {
                    $(this).css('z-index', 8);
                    $('.tooltipevent').remove();
                },
                eventAfterAllRender: function(view) {
                    $(".fc-view.fc-roomsDay-view.fc-agenda-view").css("width",120*roomCount);
                    $(".fc-view.fc-roomsWeek-view.fc-agenda-view").css("width",7*120*roomCount);
                },
                buttonText: {
                    week: '@lang('admin.view') - @lang('admin.week')',
                    day: '@lang('admin.view') - @lang('admin.day')',
                    month: '@lang('admin.month')',
                    today: '@lang('admin.today')',
                    listWeek: '@lang('admin.list')',
                    roomsDay: '@lang('admin.day')',
                    roomsWeek: '@lang('admin.week')'
                },
                monthNames: [
                    'Janvāris',
                    'Februāris',
                    'Marts',
                    'Aprīlis',
                    'Maijs',
                    'Jūnijs',
                    'Jūlijs',
                    'Augusts',
                    'Septembris',
                    'Oktobris',
                    'Novemberis',
                    'Decemberis'
                ],
                monthNamesShort: [
                    'Janvāris',
                    'Februāris',
                    'Marts',
                    'Aprīlis',
                    'Maijs',
                    'Jūnijs',
                    'Jūlijs',
                    'Augusts',
                    'Septembris',
                    'Oktobris',
                    'Novemberis',
                    'Decemberis'
                ],
                dayNames: [
                    'Svētdiena',
                    'Pirmdiena',
                    'Otrdiena',
                    'Trešdiena',
                    'Ceturtdiena',
                    'Piektdiena',
                    'Sestdiena'
                ],
                dayNamesShort: [
                    'Sv',
                    'Pr',
                    'Ot',
                    'Tr',
                    'Ce',
                    'Pk',
                    'Se'
                ],
            });
        });
    </script>
@endsection