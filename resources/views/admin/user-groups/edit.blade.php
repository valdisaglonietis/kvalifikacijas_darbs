@extends('admin.layout.main')

@section('content')
	<form id="form_general" name="form_general" method="post" action="" class="form-horizontal">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						@lang('admin.'.$title): @lang('admin.editing')
					</h3>
				</div>
			</div>

			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<button class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"  type="submit" form="form_general" value="Submit"><i class="fa fa-check-circle"></i> @lang('admin.save_and_continue') </button>
					</li>
					<li class="m-portlet__nav-item">
						<a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
					</li>
					@if(Auth::user()->super_admin)
						<li class="m-portlet__nav-item">
							<a onclick="return confirm('@lang('admin.confirm')');" href="{{ url($url.'delete/'.$userGroup->id) }}" class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-danger">@lang('admin.delete')</a>
						</li>
					@endif
				</ul>
			</div>
		</div>

		<div class="m-portlet__body">
			@if(Auth::user()->super_admin)
				<div class="m-form__group form-group row">
					<label class="col-md-2 control-label">@lang('admin.key'):</label>
					<div class="col-md-10">
						<input type="text" class="form-control" name="name" value="{{Input::old('title',$userGroup->name)}}" placeholder="" disabled>
					</div>
				</div>
			@endif
			<div class="m-form__group form-group row">
				<label class="col-md-2 control-label">@lang('admin.title'):</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="key" value="{{Input::old('title',$userGroup->key)}}" placeholder="">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!--begin::Section-->
					<div class="m-section">
						<div class="m-section__content">
							<table class="table table-striped m-table datatable">
								<thead>
								<tr>
										<th>
											@lang('admin.modules')
										</th>
										@foreach($permission_types as $permission_type)
											<th>
												@lang('admin.'.$permission_type)
											</th>
										@endforeach
								</tr>
								</thead>
								<tbody>
								@foreach($permission_modules as $permission_module)
									<tr class="checkbox_tr">
										<td>
											@lang('admin.'.$permission_module['module'])
										</td>
										@foreach($permission_types as $permission_type)
											<td>
												@if(array_key_exists($permission_type,$permission_module))
												<input type="checkbox"
													   name="permissions[{{$permission_module[$permission_type]}}]"
													   @if(in_array($permission_module[$permission_type],$permissions))checked="checked"@endif>
												@endif
											</td>
										@endforeach
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!--end::Section-->
				</div>
			</div>
		</div>

	</form>

@endsection

@section('scripts')
	<script type="text/javascript">
		{{--var actions =  {!! isset($permission_types)?json_encode($permission_types):'[]'!!}--}}
		{{--var datatable = $('#mypermissionstable').mDatatable({--}}
			{{--sortable: true,--}}
			{{--data: {--}}
				{{--type: 'local',--}}
				{{--source: {!! isset($permission_modules)?json_encode($permission_modules):'[]'!!},--}}
				{{--pageSize: 10--}}
			{{--},--}}

			{{--pagination: true,--}}

			{{--// search: {--}}
			{{--// 	input: $('#generalSearch')--}}
			{{--// },--}}

			{{--layout: {--}}
				{{--theme: 'default', // datatable theme--}}
				{{--class: '', // custom wrapper class--}}
			{{--},--}}
			{{--columns: [--}}
				{{--{--}}
				    {{--field:'module',--}}
					{{--title:'@lang('admin.module')',--}}
				{{--},--}}

				{{--@foreach($permission_types as $key => $userGroup)--}}
				{{--{--}}

					{{--field: "{{$userGroup}}",--}}
					{{--title: "@lang('admin.'.$userGroup)",--}}
                    {{--template: function (row, index, datatable) {--}}
					   {{--console.log(row);--}}
						{{--var index = 0;--}}
						{{--// console.log(index);--}}

                        {{--for(var key in row.actions)--}}
                        {{--{--}}
                            {{--// console--}}
                            {{--if(row.actions[key]=='{{$userGroup}}') index = key;--}}
                        {{--}--}}
                        {{--return '<input type="checkbox" name="checkPermissions['+index+']" checked="checked">';--}}
                    {{--}--}}
				{{--},--}}
				{{--@endforeach--}}
			{{--],--}}
		{{--});--}}

		$('.checkbox_tr').each(function () {
		    var obj = $(this);
            obj.find('input[type="checkbox"]').first().click(function () {
				if ($(this).is(':checked')) {
				    obj.find('input[type="checkbox"]').prop('checked', true);
				}else{
                    obj.find('input[type="checkbox"]').prop('checked', false);
                }
            });

        });
	</script>
@endsection

@section('css')
@endsection
