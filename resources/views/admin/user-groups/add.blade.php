@extends('admin.layout.main')

@section('content')

	<form id="form_general" name="form_general" method="post" action="" class="form-horizontal">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						@lang('admin.'.$title): @lang('admin.adding_new')
					</h3>
				</div>
			</div>

			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<button class="btn m-btn btn-focus m-btn--icon m-btn--air m-btn--pill btn-success"
							type="submit" form="form_general" value="Submit">
							<i class="fa fa-check-circle"></i>@lang('admin.save_and_continue')
						</button>
					</li>
					<li class="m-portlet__nav-item">
						<a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-metal">@lang('admin.cancel')</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="m-portlet__body">

			<div class="m-form__group form-group row">
				<label class="col-md-2 control-label">@lang('admin.title'):</label>
				<div class="col-md-10">
					<input required type="text" class="form-control" name="title" value="{{Input::old('title')}}"
					   placeholder="">
				</div>
			</div>
		</div>
	</form>

@endsection

@section('scripts')

@endsection

@section('css')
@endsection
