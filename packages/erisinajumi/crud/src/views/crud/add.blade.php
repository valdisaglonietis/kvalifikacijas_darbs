@extends('admin.layout.main')

@section('content')

    <form id="form_general" name="form_general" method="post" action="" class="form-horizontal">
        {{ csrf_field() }}
        <div class="m-portlet__head">
            <div class="m-form m-form--label-align-right m--margin-top-20">
                <div class="row align-items-center">
                    <div class="col-xl-7 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="@lang('admin.search')..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span><i class="la la-search"></i></span>
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-5 order-1 order-xl-2 m--align-right">
                        <button class="btn btn-focus m-btn m-btn--icon m-btn--air m-btn--pill btn-success" type="submit" form="form_general" value="Submit">
                            <i class="fa fa-plus"></i> @lang('admin.create')
                        </button>
                        <a href="{{ url($url) }}" class="btn m-btn m-btn--icon m-btn--air m-btn--pill btn-danger">
                            <i class="fa fa-times-circle"></i> @lang('admin.cancel')
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label" for="language" style="display: flex;flex-direction: column;justify-content: center;">@lang('admin.'.$key):</label>
                <div class="col-10">
                    <input type="{{$table_item}}" class="form-control" name="{{$key}}" value="{{Input::old($key)}}" placeholder="" id="{{$key}}">
                </div>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
@endsection

@section('css')
@endsection
