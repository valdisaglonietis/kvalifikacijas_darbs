@extends('admin.layout.main')

@section('content')

    <form id="form_general" name="form_general" method="post" action="" class="form-horizontal">
        <div class="m-portlet__head">
            <div class="m-form m-form--label-align-right m--margin-top-20">
                <div class="row align-items-center">
                    <div class="col-xl-7 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="@lang('admin.search')..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span><i class="la la-search"></i></span>
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-5 order-1 order-xl-2 m--align-right">
                        <a href="{{ url($url.'add') }}" class="btn btn-focus m-btn m-btn--icon m-btn--air m-btn--pill btn-success">
                            <span>
                                <i class="fa fa-plus"></i>
                                <span>@lang('admin.add_new')</span>
                            </span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="" id="modulesDatatable">
                <tr>
                @foreach($tableFields as $tableField)
                    <th>@lang('admin.'.$tableField['title'])</th>
                @endforeach
                </tr>
                @foreach($modules as $module)
                    <tr>
                    @foreach($tableFields as $key => $tableField)
                        <td>
                        @if($tableField['type'] == 'status')
                                <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                    <label>
                                        <input class="moduleSwitch" id="status_{{$module->id}}" data-id="{{$module->id}}"
                                               data-active="{{$module->$key == 1?'1':'-1'}}" onclick="changeModuleStatus(this)"
                                               type="checkbox" {{$module->$key == 1?'checked="checked"':''}} name="">
                                        <span></span>
                                    </label>
                                </span>
                        @else
                            {{$module->$key}}
                        @endif
                        </td>
                    @endforeach
                    </tr>
                @endforeach
            </table>

        </div>
    </form>

@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function () {

            /* Datatable */
            var modulesDataTable = $('#modulesDatatable').mDatatable({
                sortable: true,
                pagination: true,
                search: {
                    input: $('#generalSearch')
                },

                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                },
            });
            modulesDataTable.reload();
        });

        function changeModuleStatus(module){
            var button = document.getElementById(module.id);
            var status = button.getAttribute('data-active')*-1;
            $.ajax({
                method: 'POST',
                url: '{{url($url.'changestatus').'/'}}'+module.getAttribute('data-id')+'?status='+status,

                success: function(data){
                    button.setAttribute('data-active',status+'');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert('@lang('admin.failed_to_save')');
                },
            });
        }
    </script>
@endsection

@section('css')
@endsection
