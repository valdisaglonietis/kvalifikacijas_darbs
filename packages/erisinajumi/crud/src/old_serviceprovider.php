<?php

namespace Erisinajumi\Crud;

use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    private $packageName = 'Erisinajumi\\Crud\\Controllers\\';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views/crud', 'crud');
//        $this->registerHelpers();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make($this->packageName.'CrudController');
//        $this->app->make($this->packageName.'ApiController');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');

    }

    /**
     * Register helpers file
     */
    public function registerHelpers()
    {
        // Load the helpers in /Helpers/api.php
        if (file_exists($file = __DIR__.'/Helpers/api.php')){
            require $file;
        }
    }
}
