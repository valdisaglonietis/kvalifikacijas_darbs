<?php
namespace Erisinajumi\Crud\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Str;


class CrudModule extends Model {

    use SoftDeletes;

    protected $table = 'crud_modules';
    protected $guarded = ['id'];








    public function isActive(){
        return $this->status;
    }

    public function children(){
        return $this->hasMany('App\Models\ModulesPermissions', 'parent_id')->orderBy('order','asc');
    }

    public function existingVariables(){
        return $this->hasMany('App\Models\ModulesVariables', 'parent_id')
            ->where('deleted','0')
            ->orderBy('order','asc');
    }

    public static function addVar($name){
        $item = new Modules();
        $item->key = Str::slug($name);
        $item->status = 0;
        $item->name = $name;
        $item->link = $item->key;
        $item->deletable=1;
        $item->save();

        return $item->id;
    }

    public function deleteVariables(){
        $variables = $this->variables()->get();
        foreach($variables as $variable){
            $variable->delete();
        }
    }
}
