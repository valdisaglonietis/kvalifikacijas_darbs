<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Str;

class CrudVariable extends Model {
    protected $table = 'crud_variables';
    protected $guarded = ['id'];














    public static function addVar($variableData , $parent_id,$order){
        $variable_item = new ModulesVariables();
        $variable_item->key = $variableData['key'];
        $variable_item->type = $variableData['type'];
        $variable_item->limit = isset($variableData['limit'])?$variableData['limit']:'0';
        $variable_item->additional = isset($variableData['additional'])?$variableData['additional']:'';
        $variable_item->parent_id = $parent_id;
        $variable_item->order = $order;
        $variable_item->active = 1;
        $variable_item->generated = 0;
        $variable_item->save();
        return $variable_item->id;
    }

    public function setGenerated($value){
        $this->generated=$value;
        $this->save();
    }

    public function deleteVar(){
        $this->deleted = 1;
        $this->save();
    }

}