<?php namespace App\Http\Controllers\Core;
use App\Models\ModulesVariables;
use Illuminate\Routing\Controller as BaseController;
use App\Models\UserActivities;
use App\Models\ModulesPermissions;
use Session;
use View;
use Input;
use Str;
use Redirect;
use Validator;
use Lang;
use URL;
use Auth;
use Config;
use Artisan;
use Illuminate\Support\Facades\Response;

class ModulesController extends BaseController{
    private $url = 'admin/modules';
    private $view = 'admin.modules.';
    private $module = 'Modules';

    private  $breadcrumbs = [
        'modules'
    ];

    public function __construct(){
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->super_admin){
                return Response::view('admin.template.error',[
                    'error' => '403',
                ]);
            }

            return $next($request);
        });
    }

    public function get_index(){
        $items = ModulesPermissions::where('parent_id','0')->orderBy('order','asc')->get();

        return View::make($this->view.'index',[
            'items'=>$items,
            'module_url' => $this->url,
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }

    public function get_add(){
        return View::make($this->view.'add',[
            'module_url' => $this->url,
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }

    public function post_add(){
        $action = 'add';
        $rules = [
            'name' => 'required|unique:ModulesPermissions,key|max:255',
        ];

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())return Redirect::to($this->url.$action)->withErrors($validator)->withInput();

        $id = ModulesPermissions::addVar(Input::get('name'));

        UserActivities::register($this->module,Lang::get('admin.item_added'), $id);
        Session::put('message', Lang::get('admin.saved'));
        return Redirect::to($this->url.'/edit/'.$id);
    }

    public function get_edit($id){
        $item = ModulesPermissions::findOrFail($id);

        return View::make($this->view.'edit',[
            'item'=>$item,
            'module_url' => $this->url,
            'breadcrumbs' => $this->breadcrumbs,
            'varTypes' => Config::get('templatemodule.types'),
            'variables' => $item->existingVariables,
        ]);


    }
    public function post_edit($id){
        $item = ModulesPermissions::findOrFail($id);
        $variables['new'] = (Input::has('variables'))? $variables = Input::get('variables'):[];
        $variables['old'] = (Input::has('existing_variables'))? $existing_variables = Input::get('existing_variables'):'';
        $rules = $this->validatorForModuleVariables($variables['new'],$variables['old']);
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) return Redirect::to($this->url.'/edit/'.$id)->withErrors($validator)->withInput();
        $item->name = Input::get('name');
        (Input::has('section'))?$item->section = 1:$item->section = 0;
        (Input::has('status'))?$item->status = 1:$item->status = 0;

        $item->save();


        foreach($item->existingVariables as $existingVariable){
            $existingVariable->update([
                'active' => $existing_variables[$existingVariable->id]['active'],
            ]);
        }

        foreach($variables as $key => $variable){
            ModulesVariables::addVar($variable,$id,count($existing_variables)+1+$key);
        }


        if($item->section == 0){
            $name = $this->generateMigration($item,1);

            if($exitcode = $this->performMigration($name)){
                Session::put('message', Lang::get('admin.module_generated'));
            }else{
                Session::put('message', Lang::get('admin.failed_to_generate_migration')." - ".$exitcode);
                $working = false;
            }
            $item->migration_generated = 1;
            $item->save();
        }

        UserActivities::register($this->module,Lang::get('admin.module_generated'), $item->id);
        Session::put('message', Lang::get('admin.module_generated'));

        return Redirect::to($this->url.'/edit/'.$id);
    }

    public function get_delete($id){
        $item = ModulesPermissions::findOrFail($id);
        if($item->deletable){
            $item->status = 0;
            $item->key = $item->key."_old_variable_".$item->id;
            $item->save();
            $item->delete();
        }

        return Redirect::to($this->url);
    }

    public function post_updateposition(){
        $list = Input::get('list');
        if(!empty($list)){
            $i = 0;
            foreach($list as $list_item){
                $this->recursiveUpdatePosition($list_item,$i,0);
                $i++;
            }
            UserActivities::register($this->module, Lang::get('admin.changed_order'), 0);
        }
    }

    private function recursiveUpdatePosition($list_item,$i,$parent_id){
        $item = ModulesPermissions::findOrfail($list_item['id']);
        $item->order = $i;
        $item->parent_id = $parent_id;
        $item->save();
        if(isset($list_item['children'])) {
            foreach ($list_item['children'] as $child_item) {
                $this->recursiveUpdatePosition($child_item,$i,$list_item['id']);
                $i++;
            }
        }
    }

    public function post_updatestatus($id=null){

        if(Input::has('status')){
            $item = ModulesPermissions::findOrFail($id);
            (Input::get('status') == "true")?$item->status = 1:$item->status = 0;
            $item->save();
        }


        UserActivities::register($this->module, Lang::get('admin.changed_status'), $item->id);

    }

    private function validatorForModuleVariables($variables,$existing_variables){
        // TODO add more types for rules
        $variableCount = 0;
        $variableKeys = [];
        if(count($existing_variables)){
            foreach($existing_variables as $variable){
                if($variable['active'] == 1){
                    $variableKeys[] = $variable['key'];
                }
            }
        }

        if(count($variables)){
            foreach($variables as $variable){
                if(!in_array($variable['key'],$variableKeys)){
                    $variableKeys[] = $variable['key'];
                    $variableCount++;
                }
            }
        }

        $rules = [
            'name' => 'required|max:255',
            'variables' => 'sometimes|array|between:'.$variableCount.','.$variableCount,
        ];

        foreach($variables as $key => $val){
            $rules['variables.'.$key.'.key'] = 'required|max:255';
            $rules['variables.'.$key.'.type'] = 'required';
            $rules['variables.'.$key.'.additional'] = 'sometimes|max:255';
        }
        return $rules;
    }



    // generated migration for the intended module
    private function generateMigration($item,$methodId){
        $methods = [
            'delete_old' => 0,
            'add' => 1,
        ];
        if($item == null) return false;


        // takes the migration template file and creates another migration file for the inteded module
        $template = app_path().'/Templates/TemplateMigration.php';
        $moduleAction = ($item->migration_generated == 1)?'update':'create';
        $name = date('Y_m_d_His').'_'.$moduleAction.'_'.$item->key.'_table';
        $target = app_path().'/../database/migrations/additional/'.$name.'.php';
        $moduleVariables = '';

        // gets types of variables
        $types = Config::get('templatemodule.types');
        $tableActions = Config::get('templatemodule.table_actions');
        // gets all variables of the module

        $variables_to_migrate = ModulesVariables::where('parent_id',$item->id)->where('deleted','0')->get();

        foreach($variables_to_migrate as $variable){
            if($methods['update'] == $methodId && $variable->generated == 0){
                $moduleVariables .= '$table->'.$types[$variable->type]['data_type']."('$variable->key'";
                $moduleVariables .= isset($types[$variable->type]['size'])?','.$types[$variable->type]['size']:'';
                $moduleVariables .= ')';
                $moduleVariables .= ($types[$variable->type]['default'])?"->default('".$types[$variable->type]['default']."')":'';
                $moduleVariables .= ";\n";
                $variable->setGenerated(1);
            }elseif($methods['delete_old'] == $methodId && $variable->active == 0){
                $moduleVariables .= '$table->dropColumn'."('$variable->key');\n";
            }
        }



        // words to seek in the migration template
        $seeker = [
            '/*MODULEKEY*/',
            '/*MODULEVARIABLES*/',
            '/*MODULEACTION*/',
            '/*MODULEMANDATORYBEGIN*/',
            '/*MODULEMANDATORYEND*/',
            '/*MODULEID*/',
            '/*MODULEACTIONTYPE*/',
        ];
        // if a seeker word is found it is replaced by the corresponding module array value. 1-> 1 , 2->2 and so on
        $module = [
            $item->key,
            $moduleVariables,
            $tableActions[$moduleAction]['action'],
            $tableActions[$moduleAction]['mandatory_begin'],
            $tableActions[$moduleAction]['mandatory_end'],
            $item->id,
            $moduleAction,
        ];

// opens template for reading and the new migration file for writing
        $reading = fopen($template, 'r');
        $writing = fopen($target, 'a');

        // initiates the reading and writing of template and new migraiton
        while (!feof($reading)) {
            $line = fgets($reading);

            // seeks every line and replaces seeker words with module words , line variable is used for contaning line information
            $line = str_replace($seeker, $module, $line);
            // line is written to the new file
            fputs($writing, $line);
        }
        // files are closed and job is finished
        fclose($reading);
        fclose($writing);
        // returns the new migration file name
        return $name;
    }

    // performs migration
    // TODO curently performs a folder migration, but a migration_file_name is passed as $name, so should try to perform only a single file migration
    private function performMigration($name){
        Artisan::call('migrate', ['--path' => 'database/migrations/additional', '--force' => true]);
        return true;
    }

    private function generateModuleModel($item=null){
        if($item == null) return false;
        $path = app_path().'/../app/Models/Additional/';
        $template = app_path().'/../app/Templates/TemplateModel.php';
        $newModule = $path.$item->key.'.php';

        $module = [
            $item->key,
        ];
        $seeker = [
            '/*MODULEKEY*/',
        ];

        if (!file_exists($newModule)) {
            $reading = fopen($template, 'r');
            $writing = fopen($newModule, 'w');

            while (!feof($reading)) {
                $line = fgets($reading);
                $line = str_replace($seeker, $module, $line);
                fputs($writing, $line);
            }

            fclose($reading);
            fclose($writing);
        }else{
            return false;
        }
        return true;
    }


    /**
     *
     *      DEPRECATED FUNCTIONS
     *
     */

    private function generateModuleController($item=null){
        if($item == null) return false;
        $path = app_path().'/../app/Http/Controllers/Admin/Additional/';
        $template = app_path().'/../app/Templates/TemplateController.php';
        $newModule = $path.$item->key.'Controller.php';
        $variables_add = '';
        $variables_show = '';
        $variables_edit = '';
        foreach($item->variables as $variable){
            $strToAdd = "'$variable->key' => ['type' => '".Config::get('templatemodule.types')[$variable->type]['type'] ."'],";
            if($variable->check_add) $variables_add .= $strToAdd;
            if($variable->check_show) $variables_show .= $strToAdd;
            if($variable->check_edit) $variables_edit .= $strToAdd;
        }

        $module = [
            $item->key,
            "'" . Lang::get('module.' . $item->key) . "'",
            '$breadcrumb = [\''.$item->key.'\']',
            '$variables_add = ['.$variables_add.']',
            '$variables_show = ['.$variables_show.']',
            '$variables_edit = ['.$variables_edit.']',
        ];
        $seeker = [
            '/*MODULEKEY*/',
            '/*MODULENAME*/',
            '/*BREADCRUMB*/',
            '/*VARIABLES_ADD*/',
            '/*VARIABLES_SHOW*/',
            '/*VARIABLES_EDIT*/'
        ];

        if (!file_exists($newModule)) {
            $reading = fopen($template, 'r');
            $writing = fopen($newModule, 'w');

            while (!feof($reading)) {
                $line = fgets($reading);
                $line = str_replace($seeker, $module, $line);
                fputs($writing, $line);
            }

            fclose($reading);
            fclose($writing);
        }else{
            return false;
        }
        return true;
    }

    private function includeControllerIntoRoutes($item){
        if($item == null) return false;

        $template = app_path().'/../app/Templates/TemplateIncludeRouteController.php';
        $target = app_path().'/../resources/views/admin/template/modules/includedControllers.blade.php';
        $data = '';
        $module = [
            $item->key,

        ];
        $seeker = [
            '/*MODULEKEY*/',
        ];

        $reading = fopen($template, 'r');

        while (!feof($reading)) {
            $line = fgets($reading);

            $line = str_replace($seeker, $module, $line);
            $data .=$line;
        }

        fclose($reading);

        $writing = fopen($target, 'a');
        fputs($writing, $data);
        fclose($writing);

        return true;
    }
}