<?php
namespace Erisinajumi\Crud\Controllers;

use App\Http\Controllers\Controller;
use Erisinajumi\Crud\Models\CrudModule;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;


class ApiController extends Controller
{


    public function modules($limit = 0)
    {
        $query = CrudModule::whereNull('deleted_at');
        $limit = intval($limit);
        switch($limit){
            case 0:
                $query = $query->get();
                break;
            case 1:
                $query = $query->first();
                break;
            default:
                $query = $query->limit($limit)->get();
                break;
        }

        return new JsonResponse(['body' => $query],200);
    }

}