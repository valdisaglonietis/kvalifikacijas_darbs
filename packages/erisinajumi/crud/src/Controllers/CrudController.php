<?php
namespace Erisinajumi\Crud\Controllers;

use App\Http\Controllers\Controller;
use Erisinajumi\Crud\Models\CrudModule;
use Input;

class CrudController extends Controller
{
    private $title = 'crud';
    private $action = [
        'index'     => 'list',
        'add'       => 'adding',
        'edit'      => 'editing',
    ];
    private $table_fields_index = [
        'id'        => [
            'type'  => 'text',
            'title' => 'id',
        ],
        'name'      => [
            'type'  => 'text',
            'title' => 'title',
        ],
        'status'    => [
            'type'  => 'status',
            'title' => 'status',
        ],
    ];

    public function index()
    {
        hasPermissions($this->title,'view');
        $modules = CrudModule::all();
        return view($this->title.'::index',[
            'breadcrumbs' => [$this->title],
            'title' => $this->title,
            'url' => "/$this->title/",
            'action' => $this->action['index'],
            'modules' => $modules,
            'tableFields' => $this->table_fields_index,
        ]);
    }
    public function changeStatus($id){
        hasPermissions($this->title,'edit');
        if(Input::has('status')){
            $item = CrudModule::findOrFail($id);
            $item->status = (Input::get('status') == 1)?1:0;
            $item->save();
        }
    }

    public function create(){
        hasPermissions($this->title,'edit');
        return view($this->title.'::add',[
            'breadcrumbs' => [$this->title],
            'title' => $this->title,
            'url' => "/$this->title/",
            'action' => $this->action['add'],
            'tableFields' => $this->table_fields_index,
        ]);
    }

    public function store(){
        hasPermissions($this->title,'edit');

    }

    public function show($id){
        hasPermissions($this->title,'edit');

    }
    public function update($id){
        hasPermissions($this->title,'edit');

    }

    public function delete($id){
        hasPermissions($this->title,'edit');
    }

}