<?php

///* Api*/
//Route::group(['prefix' => 'api', 'middleware' => ['web','insideApi']], function (){
//    $packageUrl = 'erisinajumi\\crud\\';
//    $controllerUrl = 'controllers\\';
//    Route::get('crudadmin/modules/{limit?}',$packageUrl.$controllerUrl.'ApiController@modules');
//});

/* Routes */


Route::group(['middleware' => ['web','user']], function() { // user - access and locale together
    $packageUrl = 'erisinajumi\\crud\\';
    $controllerUrl = 'controllers\\';
    Route::get('crud',$packageUrl.$controllerUrl.'CrudController@index');
    Route::post('crud/changestatus/{id}',$packageUrl.$controllerUrl.'CrudController@changestatus');
//    Route::get('crud/add',$packageUrl.$controllerUrl.'CrudController@create');
//    Route::post('crud/add',$packageUrl.$controllerUrl.'CrudController@store');
//    Route::get('crud/edit/{id}',$packageUrl.$controllerUrl.'CrudController@show');
//    Route::post('crud/edit/{id}',$packageUrl.$controllerUrl.'CrudController@update');
//    Route::get('crud/delete/{id}',$packageUrl.$controllerUrl.'CrudController@delete');
});
