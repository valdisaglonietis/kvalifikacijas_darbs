<?php

namespace Erisinajumi\Crud;

use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    private $packageName = 'Erisinajumi\\Crud\\Controllers\\';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views/crud', 'crud');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make($this->packageName.'CrudController');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');

    }
}
