<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCrudVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud_variables', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('key', 256);
            $table->integer('type');
            $table->integer('limit')->default('0');
            $table->string('additional',256)->default('');
            $table->integer('parent_id')->default('0');
            $table->integer('order')->default('0');
            $table->integer('generated')->default('0');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crud_variables');
    }
}