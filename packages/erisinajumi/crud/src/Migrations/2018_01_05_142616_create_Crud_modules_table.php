<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCrudModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud_modules', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('key', 256);
            $table->string('name', 256);
            $table->boolean('status');
            $table->integer('parent_id');
            $table->integer('order');
            $table->string('icon', 256);
            $table->integer('real')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crud_modules');
    }
}