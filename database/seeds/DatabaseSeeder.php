<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$users = [
            'valdis.aglonietis',
        ];
		foreach($users as $user){
            DB::table('Users')->insert([
                'first_name' => explode('.',$user)[0],
                'last_name' => explode('.',$user)[1],
                'email' => $user.'@e-r.lv',
                'username' => $user.'@e-r.lv',
                'created_at' => '2017-08-18 16:33:34',
                'updated_at' => '2017-08-18 16:33:34',
                'super_admin' => '1',
                'active' => '1',
                'password' => bcrypt($user),
            ]);
        }
	}
}
